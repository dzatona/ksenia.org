<?php

if(!empty($_REQUEST)) {
    foreach ($_REQUEST as $field => $value) {
        if ($value) {
            $conditions[] = "$field LIKE '%$value%'";
        }
    }
}

$query = 'SELECT * FROM books';

if (!empty($conditions)) {
    if (sizeof($conditions)) {
        $query .= ' WHERE ' . implode(' AND ', $conditions);
    }
}

$a = new PDO('mysql:host=localhost;dbname=ksu;charset=utf8', 'ksu', 'password');
$b = $a->query($query);

?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>База книг</title>
    <link rel="stylesheet" href="semantic.min.css" />
</head>
<body>

<div class="ui vertical masthead aligned segment">
<div class="ui container">
<form id="myForm" action="index.php" method="post">
        <table class="ui celled table">
            <thead>
                <tr>
                    <th>Название книги</th>
                    <th>Автор</th>
                    <th>Год</th>
                    <th>ISBN</th>
                    <th>Жанр</th>
                </tr>
            </thead>
            <tr>
                <td>
                    <div class="ui input">
                        <input type="text" name="name" placeholder="Укажите название" value="<?php if (!empty($_REQUEST['name'])) echo $_REQUEST['name'] ?>">
                    </div>
                </td>
                <td>
                    <div class="ui input">
                        <input type="text" name="author" placeholder="Укажите автора" value="<?php if (!empty($_REQUEST['author'])) echo $_REQUEST['author'] ?>">
                    </div>
                </td>
                <td>
                    <div class="ui input">
                        <input type="text" name="year" placeholder="Укажите год" value="<?php if (!empty($_REQUEST['year'])) echo $_REQUEST['year'] ?>">
                    </div>
                </td>
                <td>
                    <div class="ui input">
                        <input type="text" name="isbn" placeholder="Укажите ISBN" value="<?php if (!empty($_REQUEST['isbn'])) echo $_REQUEST['isbn'] ?>">
                    </div>
                </td>
                <td>
                    <div class="ui input">
                        <input type="text" name="genre" placeholder="Укажите жанр" value="<?php if (!empty($_REQUEST['genre'])) echo $_REQUEST['genre'] ?>">
                    </div>
                </td>
            </tr>
            <?php
            while ($row = $b->fetch()) {
                echo '<tr>';
                echo '<td>' . $row['name'] . '</td>';
                echo '<td>' . $row['author'] . '</td>';
                echo '<td>' . $row['year'] . '</td>';
                echo '<td>' . $row['isbn'] . '</td>';
                echo '<td>' . $row['genre'] . '</td>';
                echo '</tr>';
            }
            ?>
        </table>
        <button type="submit" class="ui primary button">
            Поиск
        </button>
    </form>
</div>
</div>

</body>
</html>