<?php

define('db', __DIR__ . '/db.json');

function getData() {

    echo '<span class="blue">&rarr;</span> Определяем наличие файла базы данных...<br />';

    if (!file_exists(db)) {
        die('<span class="blue">&rarr;</span> Файл базы данных не найден. Программа завершена.');
    } else {
        echo '<span class="blue">&rarr;</span> Файл базы данных найден, подключаем...<br />';
    }

    $db = file_get_contents(db);
    $array = json_decode($db, true);

    if (!empty($array)) {
        return $array;
    }

    return [];

}