<?php include_once 'functions.php'; ?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Телефонная книга (Задание №5)</title>
    <link rel="stylesheet" href="z5.css">
</head>
<body>
    <table class="table_dark">
        <th>Имя</th>
        <th>Фамилия</th>
        <th>Адрес</th>
        <th>Телефон</th>
        <?php foreach (getData() as $value): ?>
            <tr>
                <td><?= $value['firstName'] ?></td>
                <td><?= $value['lastName'] ?></td>
                <td><?= $value['address'] ?></td>
                <td><?= $value['phoneNumber'] ?></td>
            </tr>
        <? endforeach; ?>
    </table>
    <?php echo '<span class="blue">&rarr;</span> Данные обработаны, программа успешно завершена.<br />'; ?>
</body>
</html>