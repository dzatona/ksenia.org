<?php
/**
 * Created by PhpStorm.
 * User: Ксения
 * Date: 15.12.16
 * Time: 17:12
 */

$input = htmlspecialchars(isset($_GET['form']) ? $_GET['form'] : false);

$a = 1;
$b = 1;

?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Задание №2</title>
    <link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet">
    <style media="screen" type="text/css">
        body {
            font-family: 'PT Sans', sans-serif;
            font-size: 12px;
        }
        #form {
            border: 0;
            padding: 3px;
            outline: none;
        }
        #button {
            color: white;
            background: black;
            border: 1px solid black;
            border-radius: 3px;
        }
        #label {
            display: inline;
        }
    </style>
</head>
<body>
<div style="padding: 5px 15px; background: orange; margin: 0 0 10px 0;">
    <form action="zadanie2.php">
        <div id="label">Введите число для проверки:</div>
        <input type="text" name="form" id="form">
        <input type="submit" id="button" type="button" value="Отправить">
    </form>
</div>
<div style="padding: 5px 15px; border: 1px dashed gray; margin: 10px 0 0 0;">
    <?php
    while ( $a <= $input ) {
        if ( $a == $input ) {
            die("Задуманное число <span style='background: green; color: white; padding: 0 5px;'>ВХОДИТ</span> в числовой ряд <strong>Фибоначчи</strong>");
        } else {
            $c = $a;
            $a = $a + $b;
            $b = $c;
        }
    }
    echo "Задуманное число <span style='background: darkred; color: white; padding: 0 5px;'>НЕ ВХОДИТ</span> в числовой ряд <strong>Фибоначчи</strong>";
    ?>
</div>
</body>
</html>