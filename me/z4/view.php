<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Задание №4 (Первая версия)</title>
    <link rel="stylesheet" href="z4.css">
    <link href="//cdn.rawgit.com/noelboss/featherlight/1.6.1/release/featherlight.min.css" type="text/css" rel="stylesheet" />
    <script src="//code.jquery.com/jquery-latest.js"></script>
    <script src="//cdn.rawgit.com/noelboss/featherlight/1.6.1/release/featherlight.min.js" type="text/javascript" charset="utf-8"></script>
</head>
<body>

<?php

echo '<span class="blue">&rarr;</span> Проверяем наличие файла CSV...<br />';
if (file_exists('db.csv')) {
    echo '<span class="blue">&rarr;</span> Файл CSV найден...<br />';
    echo '<span class="blue">&rarr;</span> Начинаем чтение из файла...<br />';
} else {
    echo '<span class="blue">&rarr;</span> Файл CSV не найден, останавливаем программу...<br />';
    echo '<span class="blue">&rarr;</span> Вы можете вернуться к <a href="index.php">первому скрипту</a>.';
    echo '</body></html>';
    die();
}

?>

<table class="table_dark">
    <tr>
        <th><strong>Имя файла</strong></th>
        <th><strong>Размер файла</strong></th>
        <th><strong>Последнее изменение файла</strong></th>
        <th><strong>Превью файла</strong></th>
    </tr>

    <?php

    $db = file('db.csv');

    $array = [];
    foreach ($db as $line) {
        $x = explode(';', $line);
        $array[] = $x;
    }

    $name   = array_column($array, 0);
    $size   = array_column($array, 1);
    $change = array_column($array, 2);

    $final = [];
    for ($i=0; $i < count($name); $i++) {
        echo '<tr><td>' . $name[$i] . '.jpg</td><td>' . $size[$i] . '</td><td>' . $change[$i] . '</td><td><a href="img/' . $name[$i] . '.jpg" data-featherlight="image"><img src="img/thumbs/' . $name[$i] . '_small.jpg"></a></td></tr>';
    }

    ?>

</table>

<?php

echo '<span class="blue">&rarr;</span> Выполнение программы завершено успешно...<br />';
echo '<span class="blue">&rarr;</span> Вы можете вернуться к <a href="index.php">первому скрипту</a>.';

?>

</body>
</html>