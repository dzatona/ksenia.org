<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Задание №4 (Первая версия)</title>
    <link rel="stylesheet" href="z4.css">
</head>
<body>
<?php

setlocale(LC_TIME, "ru_RU.UTF-8");

echo '<span class="blue">&rarr;</span> Программа запущена, кодировка установлена в UTF-8...<br />';
echo '<span class="blue">&rarr;</span> Проверяем наличие CSV файла...<br />';

if (file_exists('db.csv')) {

    echo '<span class="blue">&rarr;</span> CSV файл найден, производим очистку файла...<br />';
    file_put_contents('db.csv', '');

    echo '<span class="blue">&rarr;</span> Запускаем цикл сканирования директории IMG...<br />';
    for ($i=1; $i <= count(glob('img/*.jpg')); $i++) {

        echo '<div class="item"><span class="green">&rarr;</span> <span class="file">Найден файл с именем «' . $i . '.jpg»</span><br />';

        $path   = 'img/' . $i . '.jpg';
        $tpath  = 'img/thumbs/' . $i . '_small.jpg';
        $f      = stat($path);
        $fname  = $i;
        $ftime  = strftime('%A, %d %B %G', $f['mtime']);
        $fsize  = ceil($f['size']/1000) . ' Кб';
        $data   = $fname . ';' .  $fsize . ';' .  $ftime . "\n";

        echo '<span class="green">&rarr;</span> Проверяем, сгенерирована ли для файла превьюшка...<br />';

        if (!file_exists($tpath)) {

            echo '<span class="green">&rarr;</span> Превьюшка не обнаружена, создаем...<br />';

            $thumb  = new Imagick($path);
            $thumb->resizeImage(250, 0, 0, 1);
            $thumb->writeImage($tpath);
            unset($thumb);

        } else {
            echo '<span class="green">&rarr;</span> Превьюшка уже создана, пропускаем...<br />';
        }

        echo '<span class="green">&rarr;</span> Записываем информацию о файле в CSV файл...</div>';
        $file = fopen('db.csv', 'a');
        fwrite($file, $data);
        fclose($file);

    }

    echo '<span class="blue">&rarr;</span> Все операции были успешно выполнены.<br /><span class="blue">&rarr;</span> Перейдите ко <a href="view.php">второму скрипту</a>.';

} else {
    echo '<span class="blue">&rarr;</span> Файл CSV отсутствует...<br /><span class="blue">&rarr;</span> Создаем файл CSV...<br /><span class="blue">&rarr;</span> Обновите страницу...';
    file_put_contents('db.csv', '');
}
?>
</body>
</html>