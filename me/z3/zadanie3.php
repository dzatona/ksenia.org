<?php
$input = [
    'Africa' => [
        'Panthera leo', 'Rhinocerotidae', 'Loxodonta africana', 'Panthera pardus', 'Okapia johnstoni'
    ],
    'America' => [
        'Ursus maritimus', 'Alligator mississippiensis', 'Puma yaguarondi', 'Mephitidae', 'Nasua'
    ],
    'Europe' => [
        'Aquila chrysaetos', 'Falco eleonorae', 'Monachus monachus', 'Glis glis', 'Meles meles'
    ],
    'Australia' => [
        'Macropus', 'Tachyglossidae', 'Sarcophilus laniarius', 'Thylacinus cynocephalus', 'Ornithorhynchus anatinus'
    ],
    'Antarctica' => [
        'Spheniscidae', 'Procellariidae', 'Cryolophosaurus ellioti', 'Lobodon carcinophagus', 'Hydrurga leptony'
    ]
];
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Задание №3</title>
    <link rel="stylesheet" href="z3.css">
</head>
<body>
<?php
foreach($input as $continent => $animals) {
    foreach($animals as $animal)  {
        $x = explode(' ', $animal);
        if (count($x) == 2) {
            $firsts[] = $x[0];
            $seconds[$x[1]] = $continent;
            $names[$continent] = [];
        }
    }
}
$secondKeys = array_keys($seconds);
shuffle($firsts); shuffle($secondKeys);
for($i=0; $i < count($secondKeys); $i++) {
    $continent  = $seconds[$secondKeys[$i]];
    $names[$continent][] = $firsts[$i] . ' ' . $secondKeys[$i];
}
foreach ($names as $continent => $animals) {
    echo '<div class="continent_head ' . strtolower($continent) . '">';
    echo '<h2>Животные континента &laquo;' . $continent . '&raquo;:</h2>';
    echo '</div>';
    echo '<div class="animals"><div class="circle"><span class="inside">&rarr;</span></div> ' . implode(', ', $animals) . '</div>';
}
echo '<div class="copyleft">© 2016—∞ <strong>Hardcoded and styled by Ksu</strong></div>';
?>
</body>
</html>