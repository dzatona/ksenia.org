<?php
/**
 * Created by PhpStorm.
 * User: Ксения
 * Date: 10.12.2016
 * Time: 21:29
 */

$name = 'Ксения';
$age = '26';
$mail = 'ksu@internet6.ru';
$city = 'Ростов-на-Дону';
$aboutme = 'Хочу научиться программировать =)';
?>

<!DOCTYPE>
<html lang="ru">
<head>
    <title><?php echo $name ?> - <?php echo $aboutme ?></title>
    <meta charset="utf-8">
    <style>
        body {
            font-family: sans-serif;
        }

        dl {
            display: table-row;
        }

        dt, dd {
            display: table-cell;
            padding: 5px 10px;
        }
    </style>
</head>
<body>
<h1>Страница пользователя <?php echo $name ?></h1>
<dl>
    <dt>Имя</dt>
    <dd><?php echo $name ?></dd>
</dl>
<dl>
    <dt>Возраст</dt>
    <dd><?php echo $age ?></dd>
</dl>
<dl>
    <dt>Адрес электронной почты</dt>
    <dd><?php echo $mail ?></dd>
</dl>
<dl>
    <dt>Город</dt>
    <dd><?php echo $city ?></dd>
</dl>
<dl>
    <dt>О себе</dt>
    <dd><?php echo $aboutme ?></dd>
</dl>
</body>
</html>