<?php

return [

    'basePath'  => __DIR__ . '/..',
    'appName'   => ':: ZENBook',
    'dataPath'  => __DIR__ . '/../data',
    'charset'   => 'utf8',
    'components' => [
        'log'   => [
            'logFile' => __DIR__ . '/../logs/app.log'
        ],
        'urlManager' => [

        ],
        'request' => [

        ],
        'response' => [
            // 'class' => 'zen\responses\JSONResponse'
            'class' => 'zen\responses\HttpResponse'
        ],
        'view' => [
            'class'         => 'zen\views\TwigView',
            'viewsPath'     => 'views',
            'layoutsPath'   => 'views/layouts',
            'cachePath'     => 'runtime/cache',
            'twigParams'    => [
                'debug' => true,
            ],
            'templateExtension' => 'twig'
        ],
        'db' => require ('db.php'),
        'user' => [
            'class' => 'zen\base\User',
            'userModelClass' => 'app\models\User',
        ],
    ],
    'params' => [
        'telegram_botkey' => '305522856:AAGFGBm-Yzr49kS2KSwk6sXREGwyq3OWsEc',
        'telegram_url'    => 'https://ksenia.org/bot/webupdates',
        'telegram_name'   => 'ZENFaq_bot',
    ]
];