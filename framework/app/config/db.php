<?php

    return [
        'class' => 'zen\db\PDODatabase',
        'credentials' => [
            'dsn' => 'mysql:host=zen-mysql;dbname=faq',
            'username'  => 'zen',
            'password' => 'zenpassword',
            'charset' => 'utf8' ,
        ]
    ];