<?php

namespace app\components;

use Zen;
use zen\base\Controller;

class BackendController extends Controller
{
    public $layout = 'backend';

    public function init()
    {
        if (!Zen::$app->user->isLogged()) {
            $this->redirect(Zen::$app->user->loginUrl);
            return;
        }
        parent::init();
    }
}