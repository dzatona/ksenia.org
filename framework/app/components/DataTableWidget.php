<?php

namespace app\components;

use zen\base\Widget;

class DataTableWidget extends Widget
{
    public $data = [];
    public $attributes = [];

    public function run()
    {
        return $this->render('data_table');
    }
}