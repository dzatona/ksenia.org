<?php

namespace app\controllers;

use app\models\User;
use Zen;
use app\components\BackendController;
use zen\base\Response;
use zen\exceptions\NotFoundHttpException;


class UsersController extends BackendController
{

    public $layout = 'backend';

    /**
     * @param $action
     * @param $params
     * @return Response|false
     */
    public function actionIndex()
    {
        $users = User::findAll([], [
            'start' => 0,
            'limit' => 10
        ]);

     //   print_r($users);

        return $this->render('index', ['users' => $users]);
    }

    public function actionRemove($id=null)
    {
        if ($id!=Zen::$app->user->getId()) {
            if ($id && ($user = User::findById($id))) {
                Zen::$app->log->write(Zen::$app->user->current->username . ' удалил пользователя "' . $user->username . '" (' . $user->id . ')');

                $user->remove();
            }
        }
        $this->redirect('/users/index');
    }

    public function actionCreate()
    {
        $request = Zen::$app->getRequest();
        $error = false;
        $userData = [];

        if ($request->isPost() && ($postData = $request->getPostData()) && isset($postData['User']) && ($userData=$postData['User']) )
        {
            if (isset($userData['username']) &&  isset($userData['password']) && $userData['username'] && $userData['password']) {

                $model = new User();
                $model->username = $userData['username'];
                $model->password = password_hash($userData['password'], PASSWORD_DEFAULT );
                $model->save();

                Zen::$app->log->write(Zen::$app->user->current->username . ' создал пользователя "'.$model->username.'"');

                return $this->redirect('/users/index');

            } else {
                $error = 'Both username and password fields must be filled';
            }

        }
        return $this->render('create', ['error' => $error, 'userData' => $userData]);
    }

    public function actionUpdate($id)
    {
        $request = Zen::$app->getRequest();

        $error = false;

        if (!($user = User::findById($id))) {
            throw new NotFoundHttpException('User not found');
        }

        if ($request->isPost() && ($postData = $request->getPostData()) && isset($postData['User'])) {
            if(isset($postData['User']['username']) && $postData['User']['username']) {
                $user->username = $postData['User']['username'];

                if(isset($postData['User']['password']) && $postData['User']['password']) {
                    $user->password =password_hash($postData['User']['password'],PASSWORD_DEFAULT);
                }
                Zen::$app->log->write(Zen::$app->user->current->username . ' отредактировал пользователя "'.$user->username.'" ('.$user->id.')');
                $user->save();
                return $this->redirect('/users/index');
            } else {
                $error = "Username field must be filled";
            }
        }

        $username = $user->username;


        return $this->render('update', ['error' => $error, 'username' => $username]);
    }
}