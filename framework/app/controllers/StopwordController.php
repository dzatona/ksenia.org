<?php

namespace app\controllers;

use app\models\Stopword;
use Zen;
use app\components\BackendController;
use zen\base\Response;
use zen\exceptions\NotFoundHttpException;


class StopwordController extends BackendController
{

    public $layout = 'backend';

    /**
     * @param $action
     * @param $params
     * @return Response|false
     */
    public function actionIndex()
    {
        $words = Stopword::findAll([], [
        ]);

        return $this->render('index', ['words' => $words]);
    }

    public function actionRemove($id=null)
    {
        if ($id && ($category = Stopword::findById($id)))
        {
            Zen::$app->log->write(Zen::$app->user->current->username . ' удалил стоп-слово "'.$category->name.'"');
            $category->remove();
        }
        $this->redirect('/stopword/index');
    }

    public function actionCreate()
    {
        $request = Zen::$app->getRequest();
        $error = '';

        $model = new Stopword();

        if ($request->isPost() && ($postData = $request->getPostData()))
        {

            if (!isset($postData['Stopword']['word']) || !($model->word = mb_strtolower($postData['Stopword']['word']))) {
                $error = 'Stopword name must be specified.';
            }


            if (!$error) {
                $model->createdby_id = Zen::$app->user->id;
                Zen::$app->log->write(Zen::$app->user->current->username . ' создал стоп-слово "'.$model->word.'"');
                $model->save();
                return $this->redirect('/stopword/index');
            }
        }
        return $this->render('create', ['error' => $error, 'word' => $model]);
    }

    public function actionUpdate($id)
    {
        $request = Zen::$app->getRequest();

        $error = false;

        if (!($model = Stopword::findById($id))) {
            throw new NotFoundHttpException('Stopword not found');
        }

        if ($request->isPost() && ($postData = $request->getPostData())) {
            if (!isset($postData['Stopword']['word']) || !($model->word =mb_strtolower($postData['Stopword']['word']))) {
                $error = 'Stopword name must be specified.';
            }

            if (!$error) {
                $model->save();
                Zen::$app->log->write(Zen::$app->user->current->username . ' изменил стоп-слово "'.$model->word.'" ('.$model->id.')');
                return $this->redirect('/stopword/index');
            }
        }

        return $this->render('update', ['error' => $error, 'word' => $model]);
    }
}