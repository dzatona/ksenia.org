<?php

namespace app\controllers;

use app\models\Category;
use app\models\User;
use Zen;
use app\components\BackendController;
use zen\base\Response;
use zen\exceptions\NotFoundHttpException;


class CategoriesController extends BackendController
{

    public $layout = 'backend';

    /**
     * @param $action
     * @param $params
     * @return Response|false
     */
    public function actionIndex()
    {
        $categories = Category::findAll([], [
            'count' => [
                'questions_count' => [ 'question', ['id' => 'category_id']],
                'answered_count' => [ 'question', ['id' => 'category_id'], ['answered' => null]],
            ]
        ]);

        return $this->render('index', ['categories' => $categories]);
    }

    public function actionRemove($id=null)
    {
        if ($id && ($category = Category::findById($id)))
        {
            Zen::$app->log->write(Zen::$app->user->current->username . ' удалил тему "'.$category->name.'"');
            $category->remove();
        }
        $this->redirect('/categories/index');
    }

    public function actionCreate()
    {
        $request = Zen::$app->getRequest();
        $error = '';

        $model = new Category();

        if ($request->isPost() && ($postData = $request->getPostData()))
        {

            if (!isset($postData['Category']['name']) || !($model->name =$postData['Category']['name'])) {
                $error = 'Category name must be specified.';
            }

            if (!isset($postData['Category']['keyword']) || !($model->keyword = preg_replace('/[^\w\d]+/', '',$postData['Category']['keyword']))) {
                $error .= 'Keyword must be specified.';
            }

            if (!$error) {
                $model->createdby_id = Zen::$app->user->id;
                Zen::$app->log->write(Zen::$app->user->current->username . ' создал тему "'.$model->name.'"');
                $model->save();
                return $this->redirect('/categories/index');
            }
        }
        return $this->render('create', ['error' => $error, 'category' => $model]);
    }

    public function actionUpdate($id)
    {
        $request = Zen::$app->getRequest();

        $error = false;

        if (!($model = Category::findById($id))) {
            throw new NotFoundHttpException('Category not found');
        }

        if ($request->isPost() && ($postData = $request->getPostData())) {
            if (!isset($postData['Category']['name']) || !($model->name =$postData['Category']['name'])) {
                $error = 'Category name must be specified.';
            }

            if (!isset($postData['Category']['keyword']) || !($model->keyword = preg_replace('/[^\w\d]+/', '',$postData['Category']['keyword']))) {
                $error .= 'Keyword must be specified.';
            }

            if (!$error) {
                $model->save();
                Zen::$app->log->write(Zen::$app->user->current->username . ' изменил тему "'.$model->name.'" ('.$model->id.')');
                return $this->redirect('/categories/index');
            }
        }

        return $this->render('update', ['error' => $error, 'category' => $model]);
    }
}