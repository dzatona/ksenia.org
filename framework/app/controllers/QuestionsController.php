<?php

namespace app\controllers;

use app\models\Botuser;
use app\models\Category;
use app\models\Question;
use app\models\QuestionStopword;
use app\models\Stopword;
use unreal4u\TelegramAPI\Telegram\Methods\SendMessage;
use unreal4u\TelegramAPI\TgLog;
use Zen;
use app\components\BackendController;
use zen\base\Response;
use zen\exceptions\NotFoundHttpException;
use zen\responses\JSONResponse;


class QuestionsController extends BackendController
{

    public $layout = 'backend';

    /**
     * @param $action
     * @param $params
     * @return Response|false
     */
    public function actionIndex($category_id = null)
    {
        if (!$category_id) {
            $questions = Question::findAll(['blacklisted' => 0],[
                'with' => [
                    [ 'category', 'name', ['category_id' => 'id'] ]
                ],
                'order' => '(answered is null) DESC'
            ]);
        } else {
            $questions = Question::findAll([
                'category_id' => $category_id,
                'blacklisted' => 0
            ], [
                'with' => [
                    [ 'category', 'name', ['category_id' => 'id'] ]
                ],
                'order' => '(answered is null) DESC, created ASC'
            ]);
        }


        return $this->render('index', ['questions' => $questions, 'unanswered' => false,'category_id' => $category_id]);
    }

    /**
     * @param $action
     * @param $params
     * @return Response|false
     */
    public function actionUnanswered($category_id = null)
    {
        if (!$category_id) {
            $questions = Question::findAll([
                'answered' => null,
                'blacklisted' => 0
            ],[
                'with' => [
                    [ 'category', 'name', ['category_id' => 'id'] ]
                ],
                'order' => 'created ASC'
            ]);
        } else {
            $questions = Question::findAll([
                'category_id' => $category_id,
                'answered' => null,
                'blacklisted' => 0
            ],[
                'with' => [
                    [ 'category', 'name', ['category_id' => 'id'] ]
                ],
                'order' => 'created ASC'
            ]);
        }


        return $this->render('index', ['questions' => $questions, 'unanswered' => true,'category_id' => $category_id]);
    }

    /**
     * @param $action
     * @param $params
     * @return Response|false
     */
    public function actionBlacklisted($category_id = null)
    {
        if (!$category_id) {
            $questions = Question::findAll([
                'blacklisted' => 1
            ],[
                'with' => [
                    [ 'category', 'name', ['category_id' => 'id'] ]
                ],
                'order' => 'created ASC'
            ]);
        } else {
            $questions = Question::findAll([
                'category_id' => $category_id,
                'blacklisted' => 1
            ],[
                'with' => [
                    [ 'category', 'name', ['category_id' => 'id'] ]
                ],
                'order' => 'created ASC'
            ]);
        }

        $qq = [];
        $stopwords = Stopword::findAll();
        $questionstopwords  = QuestionStopword::findAll();
        if ($stopwords && $questionstopwords && count($stopwords) && count($questionstopwords)) {
            foreach ($questionstopwords as $qsw) {
                $qq[$qsw->question_id] = isset($qq[$qsw->question_id]) ? ($qq[$qsw->question_id] . ' ,' . $stopwords[$qsw->stopword_id]->word) : $stopwords[$qsw->stopword_id]->word;
            }
        }


        return $this->render('index', ['questions' => $questions, 'unanswered' => true,'category_id' => $category_id, 'badwords' => $qq]);
    }

    public function actionRemove($id=null)
    {
        if ($id && ($model = Question::findById($id)))
        {
            $category = Category::findById($model->category_id);
            $model->remove();
            Zen::$app->log->write(Zen::$app->user->current->username . ' удалил вопрос "'.$model->question.'" ('.$model->id.') из темы "'.$category->name.'" ('.$model->category_id.')');
        }
        $this->redirect('/questions/index');
    }

    public function actionCreate($category_id = null)
    {
        $request = Zen::$app->getRequest();
        $error = '';

        $model = new Question();
        if ($category_id) {
            $model->category_id = $category_id;
        }
        $categories = Category::findAll();

        if ($request->isPost() && ($postData = $request->getPostData()))
        {

            if (!isset($postData['Question']['category_id']) || !($model->category_id =$postData['Question']['category_id'])) {
                $error .= 'Category must be selected.';
            }


            if (!isset($postData['Question']['question']) || !($model->question =$postData['Question']['question'])) {
                $error .= 'Question must be specified.';
            }

            if (!isset($postData['Question']['answer']) || !($model->answer = $postData['Question']['answer'])) {
                $error .= 'Answer must be specified.';
            }

            $model->published = (isset($postData['Question']['published']) && $postData['Question']['published']) ? 1 : 0;

            if (!$error) {
                $model->answered_id = Zen::$app->user->id;
                $model->answered = date('Y-m-d H:i:s');

                $category = Category::findById($model->category_id);
                Zen::$app->log->write(Zen::$app->user->current->username . ' создал вопрос "'.$model->question.'" в теме "'.$category->name.'" ('.$model->category_id.')');

                $model->save();
                return $this->redirect('/questions/index');
            }
        }

        return $this->render('create', ['error' => $error, 'question' => $model, 'categories' => $categories]);
    }

    public function actionUpdate($id)
    {
        $request = Zen::$app->getRequest();

        $error = '';

        $model = new Question();

        $categories = Category::findAll();

        if (!($model = Question::findById($id))) {
            throw new NotFoundHttpException('Question not found');
        }

        if ($request->isPost() && ($postData = $request->getPostData()))
        {

            if (!isset($postData['Question']['category_id']) || !($model->category_id =$postData['Question']['category_id'])) {
                $error .= 'Category must be selected.';
            }


            if (!isset($postData['Question']['question']) || !($model->question =$postData['Question']['question'])) {
                $error .= 'Question must be specified.';
            }

            if (!isset($postData['Question']['answer']) || !($model->answer = $postData['Question']['answer'])) {
                $error .= 'Answer must be specified.';
            }

            $model->published = (isset($postData['Question']['published']) && $postData['Question']['published']) ? 1 : 0;
            $model->name = (isset($postData['Question']['name']) && $postData['Question']['name']) ? $postData['Question']['name'] : '';
            $model->email = (isset($postData['Question']['email']) && $postData['Question']['email']) ? $postData['Question']['email'] : '';



            if (!$error) {
                $model->answered_id = Zen::$app->user->id;

                if (!$model->answered) {
                    $model->answered = date('Y-m-d H:i:s');
                    if($model->botuser_id) {
                        // Need to send notification
                        if (($botuser = Botuser::findById($model->botuser_id)) && $botuser->telegram_id) {

                            $key = Zen::$app->params['telegram_botkey'];
                            $tgLog = new TgLog($key);
                            $message = new SendMessage();
                            $message->chat_id = $botuser->telegram_id;
                            $message->text = 'Поступил ответ на ваш вопрос: ' . $model->answer;
                            $tgLog->performApiRequest($message);


                        }


                    }
                }
                $category = Category::findById($model->category_id);
                Zen::$app->log->write(Zen::$app->user->current->username . ' отредактировал вопрос "'.$model->question.'" ('.$model->id.') из темы "'.$category->name.'" ('.$model->category_id.')');

                $model->save();
                return $this->redirect('/questions/index');
            }
        }

        return $this->render('update', ['error' => $error, 'question' => $model, 'categories' => $categories]);
    }

    public function actionPublish($id=null)
    {
        $request = Zen::$app->getRequest();
        $response = new JSONResponse();

        if ($request->isPost() && ($postData = $request->getPostData())) {
            if (!($model = Question::findById($id))) {
                throw new NotFoundHttpException('Question not found');
            }
            $model->published = intval($postData['published']);
            $category = Category::findById($model->category_id);
            Zen::$app->log->write(Zen::$app->user->current->username . ($model->published?" опубликовал":" скрыл") .' вопрос "'.$model->question.'" ('.$model->id.') из темы "'.$category->name.'" ('.$model->category_id.')');
            $model->save();
            return $response->setContent(['published' => $model->published]);
        }

        return false;
    }
}