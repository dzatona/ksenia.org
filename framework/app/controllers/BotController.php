<?php

namespace app\controllers;

use app\models\Botuser;
use app\models\Category;
use app\models\Question;
use app\models\QuestionStopword;
use unreal4u\TelegramAPI\Telegram\Methods\SendMessage;
use unreal4u\TelegramAPI\Telegram\Methods\SendPhoto;
use unreal4u\TelegramAPI\Telegram\Types\Inline\Keyboard\Button;
use unreal4u\TelegramAPI\Telegram\Types\Inline\Keyboard\Markup;
use unreal4u\TelegramAPI\Telegram\Types\KeyboardButton;
use unreal4u\TelegramAPI\Telegram\Types\ReplyKeyboardMarkup;
use \unreal4u\TelegramAPI\TgLog;
use \unreal4u\TelegramAPI\Telegram\Methods\SetWebhook;
use \unreal4u\TelegramAPI\Telegram\Types\Update;
use \unreal4u\TelegramAPI\Telegram\Types\Custom\InputFile;

use Zen;
use zen\base\Controller;

class BotController extends Controller
{
    public $layout = 'base';

    public function actionRegister()
    {
        $url = Zen::$app->params['telegram_url'];
        $key = Zen::$app->params['telegram_botkey'];
        $name = Zen::$app->params['telegram_name'];

        $setWebhook = new SetWebhook();
        $setWebhook->url = $url;

        $tgLog = new TgLog($key);
        $tgLog->performApiRequest($setWebhook);
        return $this->render('registered', ['botname' => $name]);
    }

    public function actionWebupdates()
    {
        $key = Zen::$app->params['telegram_botkey'];

        $updateData = json_decode(file_get_contents('php://input'), true);
        $update = new Update($updateData);

        file_put_contents('/tmp/log2', print_r($update, true));

        if (isset($update->callback_query) && !empty($update->callback_query)) {

            $fio = $update->callback_query->from->first_name . ' ' . $update->callback_query->from->last_name;
            $chat_id = $update->callback_query->message->chat->id;
        } else {
            $fio = $update->message->from->first_name . ' ' . $update->message->from->last_name;
            $chat_id = $update->message->chat->id;
        }


        if (!($botUser = Botuser::findOneByAttributes(['telegram_id' => $chat_id]))) {
            $botUser = new Botuser();
            $botUser->telegram_id = $chat_id;
            $botUser->state_id = Botuser::BOT_STATE_WELCOME;
        }


        $tgLog = new TgLog($key);

        if (!isset($update->callback_query)) {
            $text = $update->message->text;

            switch ($text) {
                case "Задать вопрос":
                    $message = new SendMessage();
                    $message->chat_id = $chat_id;

                    $botUser->state_id = Botuser::BOT_STATE_ASK;
                    $keyboard = $this->getCategories();

                    $message->disable_web_page_preview = true;
                    $message->text = "Пожалуйста, выберите категорию, наиболее подходящую под ваш вопрос:";
                    $message->parse_mode = 'Markdown';
                    $message->reply_markup = $keyboard;


                    $tgLog->performApiRequest($message);

                    break;
                case "Посмотреть FAQ":
                    $botUser->state_id = Botuser::BOT_STATE_FAQ;
                    $message = new SendMessage();
                    $message->chat_id = $chat_id;

                    $botUser->state_id = Botuser::BOT_STATE_FAQ;
                    $keyboard = $this->getCategories();

                    $message->disable_web_page_preview = true;
                    $message->text = "Пожалуйста, выберите категорию вопросов, интересующую Вас:";
                    $message->parse_mode = 'Markdown';
                    $message->reply_markup = $keyboard;


                    $tgLog->performApiRequest($message);
                    break;
                default:

                    if ($botUser->state_id == Botuser::BOT_STATE_ASKDO) {
                        $message = new SendMessage();
                        $message->text = 'Спасибо за Ваш вопрос! Учтите, обработка вопроса администратором занимает некоторое время, ожидайте ответа.';
                        $message->chat_id = $chat_id;
                        $tgLog->performApiRequest($message);
                        $botUser->state_id = Botuser::BOT_STATE_WELCOME;

                        $question = new Question();
                        $question->category_id = $botUser->category_id;
                        $question->published = 0;
                        $question->name = $fio;
                        $question->email = '';
                        $question->question = htmlspecialchars(strip_tags($text), ENT_QUOTES, "UTF-8");
                        $question->botuser_id = $botUser->id;

                        $stopids = $question->checkStopWords($question->question);

                        $question->blacklisted = count($stopids) ? 1 : 0;

                        $question->save();

                        if (count($stopids)) {
                            foreach ( $stopids as $stopid) {
                                $qsw = new QuestionStopword();
                                $qsw->question_id = $question->id;
                                $qsw->stopword_id = $stopid;
                                $qsw->save();
                            }
                        }



                    } else {
                        $this->sendWelcome( $tgLog, $chat_id, $fio);
                    }


            }

        } else  {
            $params = explode('=',$update->callback_query->data);
            $message = new SendMessage();
            $message->chat_id = $chat_id;

            if ($params[0] == 'category_id') {
                $category_id = $botUser->category_id = intval($params[1]);

                if (!($category = Category::findById($category_id))) {
                    $message->text = "Sorry, could not find specified category ";
                    $botUser->state_id = Botuser::BOT_STATE_WELCOME;
                    $tgLog->performApiRequest($message);

                } else {
                    if ($botUser->state_id == Botuser::BOT_STATE_ASK) {
                        // Category for question is good, ask
                        $botUser->state_id = Botuser::BOT_STATE_ASKDO;
                        $message->text = "Ок, теперь введите Ваш вопрос для категории '" . $category->name . "' ";
                        $tgLog->performApiRequest($message);
                    } elseif ($botUser->state_id == Botuser::BOT_STATE_FAQ) {
                        // Category for faq is selected
                        $keyboard = $this->getQuestions($category_id);

                        $message->disable_web_page_preview = true;
                        $message->text = "Выберите вопрос, на который желаете посмотреть ответ:";
                        $message->parse_mode = 'Markdown';
                        $message->reply_markup = $keyboard;
                        $tgLog->performApiRequest($message);
                    }


                }
            } else {
                // Question display
                $question_id =  intval($params[1]);
                if ($question = Question::findById($question_id)) {
                    $message->text = $question->answer;
                    $botUser->state_id = Botuser::BOT_STATE_WELCOME;
                    $tgLog->performApiRequest($message);
                    $this->sendWelcome($tgLog, $chat_id, $fio);
                } else {
                    $message->text = "Sorry, could not find specified question";
                    $botUser->state_id = Botuser::BOT_STATE_WELCOME;
                    $tgLog->performApiRequest($message);
                    $this->sendWelcome($tgLog, $chat_id, $fio);
                }
            }


        }


        $botUser->save();

//        file_put_contents('/tmp/log', $updateData);

    }

    private function getCategories()
    {
        $inlineKeyboard = new Markup();
        $categories = Category::findAll();

        foreach ($categories as $category) {
            $inlineKeyboardButton = new Button();
            $inlineKeyboardButton->text = $category->name;
            $inlineKeyboardButton->callback_data = 'category_id='.(string)$category->id;
            $inlineKeyboard->inline_keyboard[][] = $inlineKeyboardButton;
        }

        return $inlineKeyboard;
    }

    private function getQuestions( $category_id)
    {
        $inlineKeyboard = new Markup();
        $questions = Question::findAll(['published' => 1, 'category_id' => $category_id]);

        foreach ($questions as $question) {
            $inlineKeyboardButton = new Button();
            $inlineKeyboardButton->text = $question->question;
            $inlineKeyboardButton->callback_data = 'question_id='.(string)$question->id;
            $inlineKeyboard->inline_keyboard[][] = $inlineKeyboardButton;
        }

        return $inlineKeyboard;
    }

    private function sendWelcome($tgLog,$chat_id, $fio)
    {
        $photoMessage = new SendPhoto();
        $photoMessage->chat_id = $chat_id;
        $photoMessage->photo = new InputFile(Zen::$app->getBasePath() . '/../web/images/bender.jpg');
        $photoMessage->caption = 'Привет, ' . $fio . '!  Выберите действие:';

        $photoMessage->reply_markup = new ReplyKeyboardMarkup();
        $photoMessage->reply_markup->one_time_keyboard = true;

        $keyboardButton = new KeyboardButton();
        $keyboardButton->text = 'Задать вопрос';
        $photoMessage->reply_markup->keyboard[0][] = $keyboardButton;

        $keyboardButton = new KeyboardButton();
        $keyboardButton->text = 'Посмотреть FAQ';
        $photoMessage->reply_markup->keyboard[0][] = $keyboardButton;
        $tgLog->performApiRequest($photoMessage);
    }


}