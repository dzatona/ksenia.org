<?php

namespace app\controllers;

use app\models\Category;
use app\models\Question;
use app\models\QuestionStopword;
use app\models\User;
use Zen;
use zen\base\Controller;
use zen\base\Response;


class SiteController extends Controller
{

    public $layout = 'base';

    /**
     * @param $action
     * @param $params
     * @return Response|false
     */
    public function actionIndex()
    {
        $categories = Category::findAll();
        $questions  = [];

        $publishedQuestions = Question::findAll(['published' => 1, 'blacklisted' => 0]);
        if ($publishedQuestions) {
            foreach ($publishedQuestions as $question) {
                $questions[$question->category_id][] = $question;
            }
        }

        return $this->render('index',['categories' => $categories, 'questions' => $questions]);
    }

    public function actionLogin()
    {
        if (Zen::$app->user->isLogged()){
            $this->redirect('/');
        }

        $request = Zen::$app->getRequest();
        $username = $password = '';

        if ($request->isPost())
        {
            $postData = $request->getPostData();

            // Get the posted user credentials
            $username = isset($postData['username']) ? preg_replace('/[^\w\d]+/', '', $postData['username']) : '';
            $password = isset($postData['password']) ? preg_replace('/[^\w\d]+/', '', $postData['password']) : '';

            // if both are not empty - process with login
            if ($username && $password) {
                if (Zen::$app->user->login($username, $password)) {
                    $this->redirect('/');
                }
            }
        }

        return $this->render('login',['hasErrors' => $request->isPost(), 'username' => $username]);
    }

    public function actionLogout()
    {
        if (Zen::$app->user->isLogged()){
            Zen::$app->user->logout();
        }

        $this->redirect('/');
    }

    public function actionAsk()
    {
        $request = Zen::$app->getRequest();
        $question = new Question();
        $categories = Category::findAll();
        $error = '';


        if($request->isPost()) {
            if (($postData = $request->getPostData()) && ($postData = $postData['Question'])) {

                $category_id = isset($postData['category_id']) ? intval($postData['category_id']) : null;
                $name  = isset($postData['name']) ? htmlspecialchars(strip_tags($postData['name']), ENT_QUOTES, "UTF-8") : null;
                $email  = isset($postData['email']) ? filter_var($postData['email'], FILTER_SANITIZE_EMAIL) : null;
                $q = isset($postData['question']) ? htmlspecialchars(strip_tags($postData['question']), ENT_QUOTES, "UTF-8") : null;

                if (!Category::findById($category_id)) {
                    $error.='Category must be selected.';
                }

                if (!$name) {
                    $error.='Your name must be specified.';
                }

                if (!$email) {
                    $error.='Email address must be specified.';
                }

                if (!$q) {
                    $error.='Question must be specified.';
                }

                $question->category_id = $category_id;
                $question->published = 0;
                $question->name = $name;
                $question->email = $email;
                $question->question = $q;

                $stopids = $question->checkStopWords($question->question);

                $question->blacklisted = count($stopids) ? 1 : 0;

                if (!$error) {

                    $question->save();

                    if (count($stopids)) {
                        foreach ( $stopids as $stopid) {
                            $qsw = new QuestionStopword();
                            $qsw->question_id = $question->id;
                            $qsw->stopword_id = $stopid;
                            $qsw->save();
                        }
                    }

                    $this->redirect('/site/asked');
                }
            }
        }

        return $this->render('ask', ['ask' => $question, 'categories' => $categories, 'errors' => $error]);
    }


    public function actionAsked()
    {
        return $this->render('asked', []);
    }
}