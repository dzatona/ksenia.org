<?php
namespace app\models;

use zen\models\DatabaseModel;

/**
 * Class Question to stopword
 *
 * Represents a faq category
 *
 * @property integer id Uniq identifier
 * @property integer question_id Question identifier
 * @property integer stopword_id Stopword identifier
 * @package app\models
 */
class QuestionStopword extends DatabaseModel {

}