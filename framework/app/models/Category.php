<?php
namespace app\models;

use zen\models\DatabaseModel;

/**
 * Class Category
 *
 * Represents a faq category
 *
 * @property integer id Uniq category identifier
 * @property string name Category name
 * @property integer created Category creation timestamp
 * @property integer updated Category last update timestamp
 * @package app\models
 */
class Category extends DatabaseModel {

}