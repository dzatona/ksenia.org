<?php
namespace app\models;

use zen\models\DatabaseModel;

class Question extends DatabaseModel {

    public $category = '';

    public function checkStopWords($text)
    {
        $stopids = [];
        $word = mb_strtolower($text);
        $blacklist = Stopword::findAll();

        foreach ($blacklist as $stopword)
        {
            if (strpos($word,mb_strtolower($stopword->word)) !== false) {
                $stopids[] = $stopword->id;
            }
        }

        return $stopids;

    }

}