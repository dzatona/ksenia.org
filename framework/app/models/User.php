<?php
namespace app\models;

use zen\models\DatabaseModel;

/**
 * Class User
 *
 * Represent administrative user
 *
 * @property integer id  Uniq user identifier
 * @property string username User's login
 * @property string password User's password hash
 *
 * @package app\models
 */
class User extends DatabaseModel {


}