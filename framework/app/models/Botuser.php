<?php
namespace app\models;

use zen\models\DatabaseModel;

/**
 * Class Botuser
 *
 * Represents current telegram bot chat state
 *
 * @property integer $id
 * @property integer $telegram_id Telegram's user identifier
 * @property integer $category_id Selected category
 * @property integer $state_id Current chat state identifier
 * @package app\models
 */
class Botuser extends DatabaseModel {
    const BOT_STATE_WELCOME = 0,
          BOT_STATE_ASK = 1,
          BOT_STATE_FAQ = 2,
          BOT_STATE_ASKDO = 3;

}