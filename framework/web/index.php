<?php

    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    ini_set('memory_limit', -1);
    date_default_timezone_set('Europe/Moscow');
    error_reporting(E_ALL);

    // Load composer's packages
    require(__DIR__ . '/../vendor/autoload.php');

    // Load main engine bootstrap file
    require(__DIR__ . '/../vendor/zatona/zen/Zen.php');

    // Lad config and start the application
    $config = require __DIR__ . '/../app/config/config.php';
    (new zen\core\Application($config))->run();
