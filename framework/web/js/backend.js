jQuery(document).ready(function($) {

    $('a.remove').on('click', function(e) {
        if (confirm('Do you really wish to remove item?')) {
            return true;
        } else {
            e.preventDefault();
            return false;
        }
    });

    $('input.publish').on('click', function(e){
        var check= $(e.target);
        var id = check.data('id');

        $.ajax({
            type: 'POST',
            url: '/questions/publish/' + id,
            data: {
                published: check.prop('checked')?1:0
            },
            success: function(data) {
                check.closest('tr').removeClass();
                if (data.published) {
                  //  check.closest('tr').addClass('active');
                } else {
                    check.closest('tr').addClass('warning');

                }

                console.log(data);
            },
            error: function(data) {
                alert(data.responseText);
            }
        });

            console.log(id);
                //
    });
});