-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               8.0.0-dmr - MySQL Community Server (GPL)
-- Операционная система:         Linux
-- HeidiSQL Версия:              9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры для таблица faq.botuser
DROP TABLE IF EXISTS `botuser`;
CREATE TABLE IF NOT EXISTS `botuser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `telegram_id` bigint(20) DEFAULT NULL,
  `state_id` tinyint(3) DEFAULT '0',
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `botstate_telegram_id_uindex` (`telegram_id`),
  KEY `botstate_category_id_index` (`category_id`),
  CONSTRAINT `botstate_category_id_fk` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Дамп данных таблицы faq.botuser: ~-1 rows (приблизительно)
DELETE FROM `botuser`;
/*!40000 ALTER TABLE `botuser` DISABLE KEYS */;
/*!40000 ALTER TABLE `botuser` ENABLE KEYS */;

-- Дамп структуры для таблица faq.category
DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `createdby_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `keyword` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `category_keyword_uindex` (`keyword`),
  KEY `category_createdby_id_index` (`createdby_id`),
  CONSTRAINT `category_creator__fk` FOREIGN KEY (`createdby_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы faq.category: ~-1 rows (приблизительно)
DELETE FROM `category`;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` (`id`, `name`, `createdby_id`, `created`, `updated`, `keyword`) VALUES
  (1, 'Basics', 1, '2017-03-29 11:34:49', '2017-03-29 12:57:02', 'basics'),
  (2, 'Mobile', 1, '2017-03-29 11:35:08', '2017-03-29 12:57:05', 'mobile'),
  (3, 'Account', 1, '2017-03-29 11:35:24', '2017-03-29 12:57:07', 'account'),
  (4, 'Payments', 1, '2017-03-29 11:35:39', '2017-03-29 12:57:09', 'payments'),
  (5, 'Privacy', 1, '2017-03-29 11:36:01', '2017-03-29 12:57:11', 'privacy'),
  (6, 'Delivery', 1, '2017-03-29 11:36:13', '2017-03-29 12:57:13', 'delivery');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;

-- Дамп структуры для таблица faq.question
DROP TABLE IF EXISTS `question`;
CREATE TABLE IF NOT EXISTS `question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` varchar(255) NOT NULL,
  `answer` text,
  `answered_id` int(11) DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `answered` timestamp NULL DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `published` tinyint(4) NOT NULL DEFAULT '0',
  `name` varchar(250) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  `blacklisted` tinyint(4) NOT NULL DEFAULT '0',
  `botuser_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `question_answered_id_index` (`answered_id`),
  KEY `question_answered_time` (`answered`),
  KEY `question__category_fk` (`category_id`),
  KEY `question_status_index` (`published`),
  KEY `question_blacklisted_index` (`blacklisted`),
  KEY `question_botuser_id_fk` (`botuser_id`),
  CONSTRAINT `category_answered_user__fk` FOREIGN KEY (`answered_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `question__category_fk` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `question_botuser_id_fk` FOREIGN KEY (`botuser_id`) REFERENCES `botuser` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы faq.question: ~-1 rows (приблизительно)
DELETE FROM `question`;

-- Дамп структуры для таблица faq.questionstopword
DROP TABLE IF EXISTS `questionstopword`;
CREATE TABLE IF NOT EXISTS `questionstopword` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stopword_id` int(11) DEFAULT NULL,
  `question_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `questionstopword_question_id_fk` (`question_id`),
  KEY `questionstopword_stopword_id_fk` (`stopword_id`),
  CONSTRAINT `questionstopword_question_id_fk` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `questionstopword_stopword_id_fk` FOREIGN KEY (`stopword_id`) REFERENCES `stopword` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

-- Дамп данных таблицы faq.questionstopword: ~-1 rows (приблизительно)
DELETE FROM `questionstopword`;


-- Дамп структуры для таблица faq.stopword
DROP TABLE IF EXISTS `stopword`;
CREATE TABLE IF NOT EXISTS `stopword` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `word` varchar(50) DEFAULT NULL,
  `createdby_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `stopword_word_uindex` (`word`),
  KEY `stopword__user_fk` (`createdby_id`),
  CONSTRAINT `stopword__user_fk` FOREIGN KEY (`createdby_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 COMMENT='Стоп-слова';

-- Дамп данных таблицы faq.stopword: ~-1 rows (приблизительно)
DELETE FROM `stopword`;
/*!40000 ALTER TABLE `stopword` DISABLE KEYS */;
INSERT INTO `stopword` (`id`, `word`, `createdby_id`, `created`) VALUES
  (1, 'fuck', 1, '2017-04-01 13:44:13'),
  (3, 'dick', 1, '2017-04-01 13:45:35'),
  (4, 'penis', 1, '2017-04-01 13:45:42'),
  (5, 'vagina', 1, '2017-04-01 13:46:02');
/*!40000 ALTER TABLE `stopword` ENABLE KEYS */;

-- Дамп структуры для таблица faq.user
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'User identifier',
  `username` varchar(50) CHARACTER SET latin1 NOT NULL COMMENT 'User name',
  `password` varchar(255) CHARACTER SET latin1 NOT NULL COMMENT 'Password',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_username_uindex` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Users storage';

-- Дамп данных таблицы faq.user: ~-1 rows (приблизительно)
DELETE FROM `user`;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `username`, `password`) VALUES
  (1, 'admin', '$2y$10$XdFuQ7vt8.l3FFf6uhlOBuj6kKOshibMPIbuvgLAOvAqi/hqUD/Le'),
  (2, 'tester', '$2y$10$y72xJCbFBaK2x888yxZMqeuGIIZlzlqJizM0pwrpX1HX4Ag0D.rj2');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
