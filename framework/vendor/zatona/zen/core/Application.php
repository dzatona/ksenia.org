<?php

namespace zen\core;

use Exception;
use Zen;
use zen\base\Component;
use zen\base\Controller;
use zen\base\Request;
use zen\base\Response;
use zen\base\UrlManager;
use zen\exceptions\HttpException;
use zen\exceptions\NotFoundHttpException;

class Application extends Component {

    public $components;
    private $_basePath = null;
    private $componentsContainer = [];
    public $controller = null;

    /** @var array Additional configurable params */
    public $params = [];

    /** @var string Application charset  */
    public $charset = 'utf8';

    /** @var string Application name */
    public $appName = 'Test ZEN Application';

    /** @var  string Data storage path */
    public $dataPath;

    public function  __construct( $config = [])
    {
        Zen::$app = $this;
        $this->preInit($config);
        Component::__construct($config);
    }

    public function preInit(&$config)
    {
        if (isset($config['basePath'])) {
            $this->_basePath = ($config['basePath']);
            unset($config['basePath']);
        } else {
            throw new Exception('The "basePath" configuration for the Application is required.');
        }

        // merge core components with custom components
        foreach ($this->coreComponents() as $id => $component) {
            if (!isset($config['components'][$id])) {
                $config['components'][$id] = $component;
            } elseif (is_array($config['components'][$id]) && !isset($config['components'][$id]['class'])) {
                $config['components'][$id]['class'] = $component['class'];
            }
        }
    }

    public function init()
    {
        parent::init();
        foreach ($this->components as $component => $config) {

            if (!isset($config['class'])) {
                throw new Exception("Class for component $component not specified in config file");
            }

            $className = $config['class'];

            try
            {
                $this->componentsContainer[$component] = new $className($config);
            } catch (Exception $e) {
                die("Error creating component $component from class $className: " . $e->getMessage() . " \n");
            }

            //echo "__".$component."__ = new ".$config['class']."<br/> ";
        }

    }

    public function getBasePath()
    {
        return realpath($this->_basePath);
    }

    /**
     * Returns the configuration of core application components.
     */
    public function coreComponents()
    {
        return [
            'log' => ['class' => 'zen\log\Logger'],
            'view' => ['class' => 'zen\base\View'],
            'urlManager' => ['class' => 'zen\base\UrlManager'],
            'request' => ['class' => 'zen\base\Request'],
        ];
    }

    /**
     * Returns component by name
     * @return Component
     * @throws Exception If component not found
     */
    public function getComponent($componentName)
    {
        if(!isset($this->componentsContainer[$componentName])) {
            throw new Exception("Component $componentName not found");
        }
        return $this->componentsContainer[$componentName];
    }


    public function __get($name)
    {
        if (!empty($this->componentsContainer) && isset($this->componentsContainer[$name])) {
            return $this->componentsContainer[$name];
        }

        return parent::__get($name);
    }


    public function handleRequest(Request $request)
    {
        list ($route, $params) = $request->resolve();

        $result = $this->runAction($route, $params);

        if($result === false) {
            throw new NotFoundHttpException('Action not found');
        }

        if ($result instanceof Response) {
            return $result;
        } else {

            $response = $this->getComponent('response');
            $response->content = $result;
            return $response;
        }
    }

    /**
     * @param $route
     * @param array $params
     * @return Response
     * @throws Exception
     */
    public function runAction($route, $params =[])
    {
        $parts = $this->createController($route);
        if (is_array($parts)) {

            /* @var $controller Controller */
            list($controller, $actionID) = $parts;
            $this->controller = $controller;
            $result = $controller->runAction($actionID, $params);

            return $result;
        }

        throw new Exception('Unable to resolve the request "' . $route . '".');

    }

    public function createController($route)
    {
        list ($id, $action) = explode('/', $route, 2);
        $controller = $this->createControllerByID($id);

        if (!$controller) {
            return false;
        }

        $controller-> id = $id;

        return $controller ? [$controller, $action] : false;

    }


    public function createControllerByID($id)
    {
        $className = $id;

        $className = str_replace(' ', '', ucwords(str_replace('-', ' ', $className))) . 'Controller';
        $className = ltrim('\\app\controllers\\' . str_replace('/', '\\', '')  . $className, '\\');

        if (strpos($className, '-') !== false || !class_exists($className)) {
            return null;
        }

        $controller = new $className();
        return get_class($controller) === $className ? $controller : null;
    }


    /**
     * @return \zen\base\Request
     */
    public function getRequest()
    {
        /** @var Request $request */
        $request =$this->getComponent('request');

        return $request;
    }

    /**
     * @return \zen\base\Response
     */
    public function getResponse()
    {
        /** @var Response $response*/
        $response = $this->getComponent('response');

        return $response;
    }

    /**
     * @return \zen\base\UrlManager
     */
    public function getUrlManager()
    {
        /** @var UrlManager $urlManager */
        $urlManager =$this->getComponent('urlManager');

        return $urlManager;
    }



    public function run()
    {


        try {
            /* @var Response $response */
            $response = $this->handleRequest($this->getRequest());
            $response->send();
        } catch (Exception $e) {
          if($e instanceof Exception) {
              /** @var Response $r */
              $r = $this->getComponent('response');
              $r->content = $e->getMessage();
              $r->send();
          }
        }
    }


    /**
     *  Return the application name
     * @return string Application name
     */
    public function getAppName()
    {
        return $this->appName;
    }

    /**
     * Returns the current running controller
     * @return Controller
     */
    public function getController()
    {
        return $this->controller;
    }
}