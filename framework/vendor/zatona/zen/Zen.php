<?php


class Zen {

    /* @var zen\core\Application $app */
    public static $app;

    /*
     * Object properties setup according the given array
     */
    public static function configure($object, $params)
    {
        foreach ($params as $param => $value) {
            $object->$param = $value;
        }

        return $object;
    }

    public static function autoload($className)
    {
        if ( 'zen' == substr($className, 0, 3) ) {
            $classFile = ZEN_PATH . '/' . str_replace("\\", '/', substr($className,4)) . '.php';
            if(is_file($classFile)) {
                include($classFile);
            }
        }

        if ( isset(self::$app) && 'app' == substr($className, 0, 3) ) {
            $classFile = self::$app->getBasePath() . '/' . str_replace("\\", '/', substr($className,4)) . '.php';
            if(is_file($classFile)) {
                include($classFile);
            }
        }

        return;
    }
}

define ('ZEN_PATH', __DIR__);

spl_autoload_register(['Zen', 'autoload'], true, true);