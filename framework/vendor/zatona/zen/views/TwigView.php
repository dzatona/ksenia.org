<?php

namespace zen\views;

use Twig_Environment;
use Twig_Loader_Filesystem;
use Zen;
use zen\base\View;
use zen\exceptions\ViewNotFoundException;

class TwigView extends View {

    /** @var  string Templates extesion */
    public $templateExtension = 'twig';

    public $cachePath = 'runtime/cache';

    protected $twig = null;

    /** @var array Parameters, directly passed to Twig */
    public $twigParams = [];

    public function init()
    {
        $realCachePath = Zen::$app->getBasePath() . '/' . $this->cachePath;
        if (!is_dir($realCachePath)) {
            mkdir($realCachePath, 0770, true);
        }

        $this->twigParams = array_merge($this->twigParams, [
            'cache' => $realCachePath,
            'charset' => Zen::$app->charset,
        ]);

        parent::init();
    }

    public function renderFile($viewFile, $params = [], $context = null)
    {
        if (!is_file($viewFile)) {
            throw  new ViewNotFoundException('View file ' . $viewFile. ' not found');
        }

        // Store context
        if ($context != null) {
            $this->context = $context;
        }

        return $this->renderTwigFile($viewFile, $params);
    }
    protected function renderTwigFile($viewFile, $params)
    {
        $viewsPath = Zen::$app->getBasePath() . '/' . $this->viewsPath;
        $cleanPath = str_replace($viewsPath, '', $viewFile);

        // Create twig as singleton
        if (!$this->twig) {
            $loader = new Twig_Loader_Filesystem($viewsPath);
            $this->twig = new Twig_Environment($loader, $this->twigParams);
            $this->twig->addGlobal('app', Zen::$app);

            $functions = $this->getFunctions();
            foreach ($functions as $function) {
                $this->twig->addFunction($function);
            }
        }

        if ($template = $this->twig->load($cleanPath)) {
            $params = array_merge($params, [
                'this' => $this,
                'user' => Zen::$app->user->getCurrent(),
            ]);
            return $template->render($params);
        } else
            throw new ViewNotFoundException('View file ' . $viewFile. ' not found by twig');

    }

    /**
     * Returns twig extension functions
     * @return array Twig's functions array
     */
    protected function getFunctions()
    {
        $functions = [
            new \Twig_SimpleFunction('void', function(){}),
            new \Twig_SimpleFunction('set', [$this, 'setProperty']),
        ];

        return $functions;
    }

    /**
     * Sets object property
     *
     * @param \stdClass $object
     * @param string $property
     * @param mixed $value
     */
    public function setProperty($object, $property, $value)
    {
        $object->$property = $value;
    }
}