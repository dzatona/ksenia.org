<?php
namespace zen\exceptions;

use Exception;

class ViewNotFoundException extends Exception
{
    /**
     * Constructor.
     * @param string $message error message
     * @param int $code error code
     */

    public function __construct($message = null, $code = 0)
    {
        parent::__construct($message, $code);
    }

}
