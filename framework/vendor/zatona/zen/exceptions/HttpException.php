<?php
namespace zen\exceptions;

use Exception;
use Zen;
use zen\base\Response;

class HttpException extends Exception
{
    /**
     * Constructor.
     * @param int $status HTTP status code, such as 404, 500, etc.
     * @param string $message error message
     * @param int $code error code
     */

    public function __construct($status, $message = null, $code = 0)
    {
        /** @var Response $response */
        if ($response = Zen::$app->getComponent('response'))
        {
            $response->setStatus($status, $message);
        } else {
            header("HTTP/1.1 $status $message");
        }
        parent::__construct($message, $code);
    }

}
