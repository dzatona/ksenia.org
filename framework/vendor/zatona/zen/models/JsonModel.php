<?php

namespace zen\models;

use Zen;
use zen\base\Model;

class JsonModel extends Model
{
    public static $storageName;
    public $id;

    private $_attributesData;

    public function init()
    {
        parent::init();
    }


    protected static function getStoragePath()
    {
        $storagePath =  Zen::$app->dataPath . '/' . self::getStorageName();
        if (!is_dir($storagePath)) {
            mkdir($storagePath, 0777, true);
        }
        return $storagePath;

    }

    public static function getStorageName()
    {
        return self::$storageName ? self::$storageName : strtolower(self::get_class_name(get_called_class()));
    }

    public static function findById($id)
    {
        $filePath = self::getStoragePath() . '/' . intval($id) . '.json';
        if (is_file($filePath)) {
            $attributes = file_get_contents($filePath);
            if ($attributes = json_decode($attributes, true)) {
                $class = get_called_class();
                $model = new $class();
                $model->assignValues($attributes);
                $model->id = intval($id);
                return $model;
            } else
                return false;
        }
        return false;
    }

    public static function findAll()
    {
        $models=[];
        $path =self::getStoragePath();
        $dir = opendir($path);
        while (($file = readdir($dir)) !== false) {
            if($file != '.' && $file != '..') {
                $attributes = file_get_contents($path . '/' . $file);
                if ($attributes = json_decode($attributes, true)) {
                    $class = get_called_class();
                    $model = new $class();
                    $model->assignValues($attributes);
                    $model->id = intval(basename($file, '.json'));
                    $models[$model->id] = $model;
                }
            }
        }
        closedir($dir);

        return $models;
    }

    public static function get_class_name($className)
    {
        if ($pos = strrpos($className, '\\')) return substr($className, $pos + 1);
        return $pos;
    }

    public function assignValues($attributesValues = [])
    {
        foreach ($attributesValues as $attribute => $value)
        {
            if (isset($this->_attributes[$attribute])) {
                $this->_attributesData[$attribute] = $value;
            }
        }
    }


    public function save()
    {
        // new record
        if(!$this->id) {
            $this->id = self::getNextId();
        }

        $data = [];
        foreach ($this->_attributes as $attribute => $attributeData) {
            if ($attribute != 'id') {
                $data[$attribute] = isset($this->_attributesData[$attribute]) ? $this->_attributesData[$attribute] : $this->getDefaultValue($attribute);
            }
        }

        file_put_contents(self::getStoragePath() . '/' . $this->id . '.json', json_encode($data));
    }

    public static function getNextId()
    {
        return intval(max(array_map('intval', scandir(self::getStoragePath())))) + 1;
    }



    public function getAttributeValue($name)
    {
        return isset($this->_attributes[$name]) ? $this->_attributesData[$name] : null;
    }

    public function setAttributeValue($name, $value)
    {
        if (isset($this->_attributes[$name])) {
            $this->_attributesData[$name] = $value;
        }
    }

    public function getAttributesValues()
    {
        return $this->_attributesData;
    }

    public function remove()
    {
        if ($this->id ) {
            $filePath = self::getStoragePath() . '/' . $this->id . '.json';
            if (is_file($filePath)) {
                return unlink($filePath);
            }
        }
        return false;
    }

}