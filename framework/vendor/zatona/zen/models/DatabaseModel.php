<?php

namespace zen\models;

use Zen;
use zen\base\Model;
use zen\db\PDODatabase;

class DatabaseModel extends Model
{
    /** @var string Database table name that stores model data  */
    public $table = false;

    private $_attributesData;

    public $isNew = true;

    public function getTableName()
    {
        if (false == $this->table) {
            // try to guess table name according to the current class name
            $this->table = strtolower(basename(str_replace("\\", '/', get_called_class())));
        }

        return $this->table;
    }

    public function setAttributes()
    {
        /** @var PDODatabase $db */
        $db = \Zen::$app->getComponent('db');
        return $db->getSchema($this->getTableName());
    }

    public function isSetAttribute($name)
    {
        return isset($this->_attributesData[$name]);
    }

    public function assignValues($attributesValues = [])
    {
        foreach ($attributesValues as $attribute => $value)
        {
                $this->_attributesData[$attribute] = $value;
        }
    }

    public static function findById($id)
    {
        $class = get_called_class();
        /** @var Model $model */
        $model = new $class();

        /** @var PDODatabase $db */
        $db = \Zen::$app->getComponent('db');
        $data = $db->fetchOne('SELECT * FROM ' . $db->quoteTableName($model->getTableName()) . ' WHERE `id` = :id', [':id' => $id]);
        if ($data) {
            $model->assignValues($data);
            $model->isNew = false;
            return $model;
        }

        return false;
    }

    public static function findOneByAttributes($attributes)
    {
        $class = get_called_class();

        /** @var Model $model */
        $model = new $class();

        /** @var PDODatabase $db */
        $db = \Zen::$app->getComponent('db');

        $attrs = [];
        foreach ($attributes as $attribute => $attrData)
        {
            $attrs[':' . $attribute] = $attrData;
        }

        $data = $db->fetchOne('SELECT * FROM ' . $db->quoteTableName($model->getTableName()) . ' WHERE ' . $db->buildWhere($attributes), $attributes);

        if ($data) {
            $model->assignValues($data);
            $model->isNew = false;
            return $model;
        }

        return false;
    }

    public static function findAll($attributes=[], $params =[])
    {
        $class = get_called_class();

        /** @var PDODatabase $db */
        $db = \Zen::$app->getComponent('db');

        /** @var Model $model */
        $tempModel = new $class();

        $addon = ''; $joins =''; $relatedAttributes = [];$additionalAttributes = [];


        if (isset($params['with'])) {
            foreach ($params['with'] as $with) {
                foreach ((array)$with[1] as $field) {
                    $addon .= (', '.$with[0].'.'.$field . ' as ' . ($with[0]. '_' . $field));
                }

                $joins.= (' LEFT JOIN ' . $db->quoteTableName($with[0]) . ' ON ( '. $with[0] . '.' . current($with[2]). ' = ' . key($with[2]) . ')' );
                $relatedAttributes[] = [$with[0],$field];
            }
        }

        if (isset($params['count'])) {
            foreach ($params['count'] as $counterName => $counter){
                $where = '';
                if (isset($counter[2])) {
                    $where = ' AND ' . $db->buildWhere($counter[2]);
                }
                $addon.= (",(SELECT COUNT(1) FROM " . $db->quoteTableName($counter[0])." WHERE t.".(key($counter[1]).'= '. $counter[0].  '.' .current($counter[1])).$where.")  as " . $counterName);
                $additionalAttributes[$counterName] = ['type' => 'int'];
            }
        }

        $sql = 'SELECT t.* ' . $addon . ' FROM ' . $db->quoteTableName($tempModel->getTableName()) . ' as t ' . $joins;

        if (count($attributes)) {
            $sql .= ' WHERE ' . $db->buildWhere($attributes) ;
        }

        if (isset($params['order'])) {
            $sql .= ' ORDER BY ' . $params['order'];
        }

        if (isset($params['limit'])) {
            $sql .= ' LIMIT ' . (isset($params['start'])?$params['start']:0) . ',' . $params['limit'];
        }


        $data = $db->fetchAll($sql, $attributes);

        if (count($data)) {
            $models = [];
            foreach ($data as $item) {
                /** @var Model $model */
                $model = new $class();
                if (count($relatedAttributes)) {
                    $model->addRelatedAttributes($relatedAttributes);
                }
                if (count($additionalAttributes)) {
                    $model->addAttributes($additionalAttributes);
                }

                $model->assignValues($item);
                $model->isNew = false;
                $models[$model->id] = $model;
            }
            return $models;
        }
        return false;
    }


    public function save()
    {
        /** @var PDODatabase $db */
        $db = \Zen::$app->getComponent('db');

        if ($this->isNew) {
            $sql = 'INSERT INTO ' . $db->quoteTableName($this->getTableName()) ;
            $columns = '';
            $values = '';
            foreach ($this->getAttributes() as $attribute => $data) {
                $columns.= (($columns?',':'') . $db->quoteColumnName($attribute));
                $values .= (($values?',':'') . '?');
            }
            $sql.='('.$columns.') VALUES ('.$values.')';

            $statement = $db->prepare($sql);
            $params = [];
            foreach ($this->getAttributes() as $attribute => $data) {
                $params[] = $this->getAttributeValue($attribute);
            }
            $this->isNew = false;

            $result = $statement->execute($params);
            $this->id = $db->lastInsertId();

            return $result;
        } else {
            $sql = 'UPDATE' . $db->quoteTableName($this->getTableName()) . ' SET ' ;
            $columns = [];
            $attrData = [];

            foreach ($this->_attributesData as $attribute => $data) {
                if ($attribute!='id') {
                    $columns[] = $db->quoteColumnName($attribute) . ' = :' . $attribute;
                }
                $attrData[':' . $attribute] = $data;
            }


            $sql.=implode(',', $columns);
            $sql.=' WHERE `id` = :id';

            $statement = $db->prepare($sql);
            return $statement->execute($attrData);

        }
    }

    public function remove()
    {
        if ($this->id) {
            /** @var PDODatabase $db */
            $db = \Zen::$app->getComponent('db');

            return $db->removeById($this->id, $this->getTableName());
        }
        return false;
    }

    public function getAttributeValue($name)
    {
        return isset($this->_attributesData[$name]) ? $this->_attributesData[$name] : null;
    }

    public function setAttributeValue($name, $value)
    {
        if (isset($this->_attributes[$name])) {
            $this->_attributesData[$name] = $value;
        }
    }

    public function getAttributesValues()
    {
        return $this->_attributesData;
    }

    public function addRelatedAttributes($relatedAttributes)
    {
        /** @var PDODatabase $db */
        $db = \Zen::$app->getComponent('db');

        //print_r($relatedAttributes); exit;
        foreach ( $relatedAttributes as $attribute) {
            $schema = $db->getSchema($attribute[0]);
            $this->_attributes = array_merge($this->_attributes, [$attribute[0] . '_' . $attribute[1] => $schema[$attribute[1]]]);
        }

    }

    public function addAttributes($attributes)
    {
        $this->_attributes = array_merge($this->_attributes, $attributes);
    }

}