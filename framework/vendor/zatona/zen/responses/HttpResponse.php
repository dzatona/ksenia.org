<?php
/**
 * Represents standard HTTP response
 */
namespace zen\responses;

use zen\base\Response;

class HttpResponse extends Response
{
    protected function sendHeaders()
    {
        header('Content-Type: text/html');
        header('HTTP/1.1 ' . $this->_statusCode . ' ' . $this->_statusText);
    }

    protected function sendContent()
    {
        echo $this->content;
        return $this;
    }

    public function send()
    {
        $this->sendHeaders();
        $this->sendContent();
        return $this;
    }
}