<?php
/**
 * Represents AJAX JSON-Based response
 */

namespace zen\responses;

use zen\base\Response;

class JSONResponse extends Response
{
    protected $content = [];

    protected function sendHeaders()
    {
        header('Content-Type: application/json');
        header('HTTP/1.1 ' .$this->_statusCode . ' ' .$this->_statusText);
        return $this;
    }

    public function send()
    {
        $this->sendHeaders();
        $this->sendContent();
        return $this;
    }

    protected function sendContent()
    {
        echo json_encode($this->content);
        return $this;
    }
}