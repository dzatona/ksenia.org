<?php
namespace zen\log;

use Zen;
use zen\base\Component;

class Logger extends Component
{
    public $logFile = null;

    public function init()
    {
        parent::init();
        if ($this->logFile === null) {
            $this->logFile = Zen::$app->getBasePath() . '/logs/app.log';
        }
    }


    public function write($message) {
        $f = fopen($this->logFile, 'a+');
        fwrite($f, '[' .date('d.m.Y H:i:s') . '] ' . $message . "\n");
        fclose($f);
    }

}