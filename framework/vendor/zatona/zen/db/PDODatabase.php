<?php
namespace zen\db;

use Exception;
use PDO;
use zen\base\Component;

class PDODatabase extends Component {

    /** @var array Database access credentials */
    public $credentials = [];

    /** @var  \PDO Connection */
    public $pdo = false;

    public $dbCommandClass = 'zen\db\DBCommand';

    protected $schemaCahce = [];


    public function init()
    {
        $this->ensureConnection();
        parent::init();
    }

    public function typesArray()
    {
        return [
            'varchar'  => 'string',
            'int'      => 'int',
            'datetime' => 'datetime',

        ];
    }


    public function ensureConnection()
    {
        if (!$this->isActive()) {
            if (!isset($this->credentials['dsn'])) {
                throw  new Exception('dsn credetial must be set');
            }

            if(!isset($this->credentials['username'])) {
                $this->credentials['username'] = 'root';
            }

            if(!isset($this->credentials['password'])) {
                $this->credentials['password'] = '';
            }

            $this->pdo = new \PDO($this->credentials['dsn'], $this->credentials['username'], $this->credentials['password'],
                [
                    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
                ]
                );
        }
    }


    /**
     * @return bool Check, if PDO active
     */
    public function isActive()
    {
        return $this->pdo !== false;
    }

    public function getSchema($tableName)
    {
        $schema = [];

        if(isset($this->schemaCahce[$tableName])) {
            return $this->schemaCahce[$tableName];
        }

        if(empty($tableName)) {
            return [];
        }

        if (!($dbSchema = $this->pdo->query('SHOW FULL COLUMNS FROM ' . $this->quoteTableName($tableName)))) {
            throw new Exception(print_r($this->pdo->errorInfo(),true));
        }

        $dbSchema = $dbSchema->fetchAll(PDO::FETCH_ASSOC);

        foreach($dbSchema as $column) {
            $c = [];
            $type = $this->getTypeFromColumn($column['Type']);
            $c['type'] = $type[0];

            if (isset($type[1])) {
                $c['maxlength'] = $type[1];
            }

            if($column['Comment']) {
                $c['label'] = $column['Comment'];
            }

            $schema[$column['Field']] = $c;
        }

        $this->schemaCahce[$tableName] = $schema;

        return $schema;
    }

    public function quoteTableName($tableName)
    {
        if (false !== strpos($tableName, '`')) {
            return $tableName;
        } else {
            return '`' . trim($tableName) . '`';
        }
    }

    public function quoteColumnName($columnName)
    {
        if (false !== strpos($columnName, '`')) {
            return $columnName;
        } else {
            return '`' . trim($columnName) . '`';
        }
    }

    public function quoteData($data)
    {
        return $this->pdo->quote($data);
    }

    private function getTypeFromColumn($type)
    {
        $length = false;
        $types = $this->typesArray();

        $t = explode('(', $type);

        if (isset($t[1])) {
            $length = intval($t[1]);
        }

        if (isset($types[$t[0]])) {
            $t[0] = $types[$t[0]];
        }

        return [$t[0],$length];
    }

    public function fetchAll(string $sql, $params = [])
    {
        if (!($statement = $this->pdo->prepare($sql)) || ! $statement->execute($params)) {
            throw new Exception('PDO error: ' . $this->pdo->errorInfo()[2]);
        }


        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    public function fetchOne(string $sql, $params = [])
    {
        if (!($statement = $this->pdo->prepare($sql)) || ! $statement->execute($params)) {
            throw new Exception('PDO error: ' . $this->pdo->errorInfo()[2]);
        }

        return $statement->fetch(PDO::FETCH_ASSOC);
    }



    /**
     * @param $sql
     * @return \PDOStatement
     */
    public function prepare($sql)
    {
        return $this->pdo->prepare($sql);
    }

    public function removeById($id, $tableName)
    {
        $statement = $this->pdo->prepare('DELETE FROM ' . $this->quoteTableName($tableName). ' WHERE `id` = :id');
        $result = $statement->execute([':id' => $id]);

        return $result;
    }


    public function buildWhere(&$params)
    {
        $where = '';
        foreach ($params as $n => $v) {
            if ($v === null) {
                $where .= (($where ? ' AND ' : ' ') . '`' . $n . '` IS NULL');
                unset ($params[$n]);
            } else {
                $where .= (($where ? ' AND ' : ' ') . '`' . $n . '` = :' . $n . ' ');
            }
        }
        return $where;
    }

    public function lastInsertId()
    {
        return $this->pdo->lastInsertId();

    }

}