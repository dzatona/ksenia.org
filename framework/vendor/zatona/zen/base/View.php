<?php

namespace zen\base;


use Zen;
use zen\exceptions\ViewNotFoundException;



class View extends Component {

    /** @var  string Page title */
    public $title = '';

    /** @var  Controller Controller under which render was requested */
    public $context;

    /** @var array Template params */
    public $params = [];

    /** @var  string Templates extesion */
    public $templateExtension = 'php';

    /** @var string Base views path */
    public $viewsPath   = 'views';

    /** @var string Base layouts path */
    public $layoutsPath = 'views/layouts';


    /**
     * @param $view string View name
     * @param array $params View params
     * @param Controller $context Controller under which render was requested
     * @return mixed
     */
    public function render($view, $params = [], $context = null)
    {
        $viewFile = $this->findViewFile($view);
        $content = $this->renderFile($viewFile, $params, $context);


        if( ($layoutFile = $this->findLayoutFile()) && is_file($layoutFile))
        {
            $content = $this->renderFile($layoutFile,['content' => $content], $context);
        }


        return $content;
    }


    protected function findViewFile($name)
    {
        return realpath(Zen::$app->controller->getViewsPath($this->viewsPath) . '/' . $name . '.' . $this->templateExtension);
    }

    protected function findLayoutFile()
    {
        $layout = Zen::$app->controller->getLayoutPath($this->layoutsPath);

        if (false !== $layout) {
            return realpath($layout . '.' . $this->templateExtension);
        }

        return false;
    }


    public function renderFile($viewFile, $params = [], $context = null)
    {
        if (!is_file($viewFile)) {
            throw  new ViewNotFoundException('View file ' . $viewFile. ' not found');
        }

        // Store context
        if ($context != null) {
            $this->context = $context;
        }

        return $this->renderPhpFile($viewFile, $params);
    }

    public function renderPhpFile($_file_, $_params_ = [])
    {
        ob_start();
        ob_implicit_flush(false);
        extract($_params_, EXTR_OVERWRITE);
        require($_file_);

        return ob_get_clean();
    }

    public function getTitle($skipAppName = false)
    {
        return $skipAppName ? $this->title : ( $this->title . ' ' . Zen::$app->getAppName());
    }

}