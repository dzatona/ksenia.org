<?php

namespace zen\base;

class Widget extends Component
{
    public $view = false;

    public function run()
    {

    }

    public static function widget($params = [])
    {
        $widget = new (get_called_class())($params);
        return $widget->run();
    }

    public function render($view, $params = [])
    {
        /** @var Controller $controller */
        $controller = Zen::$app->getController();
        return $controller->render($view, $params);
    }

}