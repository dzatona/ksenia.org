<?php
/*
 * Request component
 * Represents a web request
 */
namespace zen\base;

use Exception;
use Zen;
use zen\exceptions\NotFoundHttpException;

class Request extends Component
{
    // Cache
    private $_queryParams;
    private $_url;
    private $_pathInfo;
    private $_postData;


    public function init()
    {
        parent::init();
    }

    public function resolve()
    {
        $path = Zen::$app->getUrlManager()->parseRequest($this);
        if ($path !== false) {
            list ($route, $params) = $path;
            if ($this->_queryParams === null) {
                $_GET = $params + $_GET;
            } else {
                $this->_queryParams = $params + $this->_queryParams;
            }
            return [$route, $this->getQueryParams()];

        } else {
            throw new NotFoundHttpException('Page not found');
        }
    }

    /*
     * Returns cached query params
     * @return Array Array of query params
     */
    public function getQueryParams()
    {
        if ($this->_queryParams === null) {
            return $_GET;
        }

        return $this->_queryParams;
    }

    public function isPost()
    {
        return $_SERVER['REQUEST_METHOD'] == 'POST';
    }

    public function getPostData()
    {
        if ($this->_postData) {
            return $this->_postData;
        }

        return $this->_postData = $_POST;
    }

    /**
     * Resolves the path info part of the currently requested URL.
     * A path info refers to the part that is after the entry script and before the question mark (query string).
     * The starting slashes are both removed (ending slashes will be kept).
     * @return string part of the request URL that is after the entry script and before the question mark.
     * Note, the returned path info is decoded.
     * @throws Exception if the path info cannot be determined due to unexpected server configuration
     */
    protected function resolvePathInfo()
    {

        $pathInfo = $this->getUrl();

        if (($pos = strpos($pathInfo, '?')) !== false) {
            $pathInfo = substr($pathInfo, 0, $pos);
        }

        $pathInfo = urldecode($pathInfo);

        // try to encode in UTF8 if not so
        // http://w3.org/International/questions/qa-forms-utf-8.html
        if (!preg_match('%^(?:
            [\x09\x0A\x0D\x20-\x7E]              # ASCII
            | [\xC2-\xDF][\x80-\xBF]             # non-overlong 2-byte
            | \xE0[\xA0-\xBF][\x80-\xBF]         # excluding overlongs
            | [\xE1-\xEC\xEE\xEF][\x80-\xBF]{2}  # straight 3-byte
            | \xED[\x80-\x9F][\x80-\xBF]         # excluding surrogates
            | \xF0[\x90-\xBF][\x80-\xBF]{2}      # planes 1-3
            | [\xF1-\xF3][\x80-\xBF]{3}          # planes 4-15
            | \xF4[\x80-\x8F][\x80-\xBF]{2}      # plane 16
            )*$%xs', $pathInfo)
        ) {
            $pathInfo = utf8_encode($pathInfo);
        }


        if (substr($pathInfo, 0, 1) === '/') {
            $pathInfo = substr($pathInfo, 1);
        }

        return  preg_replace("/[^[:alnum:]\/]/u", '', (string) $pathInfo);
    }


    public function getUrl()
    {
        if ($this->_url === null) {
            $this->_url = $this->resolveRequestUri();
        }

        return $this->_url;
    }

    public function setUrl($value)
    {
        $this->_url = $value;
    }

    protected function resolveRequestUri()
    {
        if (isset($_SERVER['REQUEST_URI'])) {
            $requestUri = $_SERVER['REQUEST_URI'];
            if ($requestUri !== '' && $requestUri[0] !== '/') {
                $requestUri = preg_replace('/^(http|https):\/\/[^\/]+/i', '', $requestUri);
            }
        } else {
            throw new Exception('Unable to determine the request URI.');
        }

        return $requestUri;
    }

    public function getPathInfo()
    {
        if ($this->_pathInfo === null) {
            $this->_pathInfo = $this->resolvePathInfo();
        }

        return $this->_pathInfo;
    }

    public function setPathInfo($value)
    {
        $this->_pathInfo = $value === null ? null : ltrim($value, '/');
    }




}