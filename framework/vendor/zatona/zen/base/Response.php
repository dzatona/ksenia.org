<?php
namespace zen\base;

class Response extends Component
{
    public $exitStatus = 0;

    protected $content = '';

    protected $_statusCode = 200;
    protected $_statusText = 'OK';

    public function send()
    {
       echo $this-> content;
       return $this;
    }

    public function setStatus($code, $text ='')
    {
        $this->_statusCode = $code;
        $this->_statusText = $text;
        return $this;
    }

    public function redirect($location = '/')
    {
        header('Location: '. $location);
        $this->setStatus(302, 'Found');
        return $this;
    }

    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }
}