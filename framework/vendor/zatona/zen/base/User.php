<?php
namespace zen\base;

class User extends Component {

    /** @var string Login page URL to redirect to */
    public $loginUrl = '/site/login';

    /** @var string Cookie name to identify the user */
    public $idParamName = 'ZEN_ID';

    /** @var  UserModel Represents the currently logged user */
    private $_current = false;

    /** @var string Class that represents user model */
    public $userModelClass = false;

    public function init()
    {
        $this->getCurrent();
        parent::init();
    }

    /**
     * Get current logged user model, false if user is not logged
     * @return bool|UserModel Current logged user model
     */
    public function getCurrent()
    {
        if (false === $this->_current) {
            // Try to login
            $this->_current = null;
            return $this->trySession();
        }
        return $this->_current;
    }

    /**
     * Checks, when user is logged or not
     * @return bool Is user logged
     */
    public function isLogged()
    {
        return $this->getCurrent() !== null;
    }

    /**
     * Tries to renew user's authentiocation from session
     */
    protected function trySession()
    {

        if (PHP_SESSION_ACTIVE != session_status()) {
            @session_start();
        }

        if (isset($_SESSION[$this->idParamName])){
            $id = intval($_SESSION[$this->idParamName]);
            if ($id && ($user = $this->userModelClass::findById($id))) {
                $this->_current = $user;
                return $this->_current;
            }
        }
    }


    /**
     * Save current user identifier into session
     */
    protected function saveSession()
    {
        if (PHP_SESSION_ACTIVE != session_status()) {
            @session_start();
        }

        if($user = $this->getCurrent()) {
            $_SESSION[$this->idParamName] = $user->id;
        }

    }


    /**
     * Authenticates user
     * @param $username
     * @param $password
     * @return bool|UserModel User model, represented by specified credentials, false if there is no such user
     */
    public function login($username, $password)
    {
        if (!$username || !$password) {
            return false;
        }

        if ($user = $this->userModelClass::findOneByAttributes(['username' => $username])) {
            if (password_verify($password, $user->password)) {
                $this->_current = $user;
                $this->saveSession();
            }
        }

        return $this->_current;
    }

    /**
     * Destroys current admin session
     */
    public function logout()
    {
        $this->_current = false;
        @session_destroy();
    }

    /**
     * Returns current logged user identifier
     * @return int|null Current logged user ID
     */
    public function getId()
    {
        return $this->getCurrent() ? $this->getCurrent()->id : null;
    }

}