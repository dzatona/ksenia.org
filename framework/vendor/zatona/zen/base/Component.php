<?php

namespace zen\base;

use Exception;
use Zen;

class Component {

    // Component class name
    public $class;

    private $componentsContainer = [];



    public function __construct( $config = [])
    {
        if (!empty($config)) {
            Zen::configure($this, $config);
        }

        $this->init();
        return $this;
    }



    /**
     * Initializes the object.
     * This method is invoked at the end of the constructor after the object is initialized with the
     * given configuration.
     */
    public function init()
    {
    }


    /*
     * Get component property
     */
    public function __get($name)
    {
        $getter = 'get' . $name;
        if (method_exists($this, $getter)) {
            return $this->$getter();
        }

        throw new Exception('Getting unknown property: ' . get_class($this) . '::' . $name);
    }

    /*
     * Set component property
     */
    public function __set($name, $value)
    {
        $setter = 'set' . $name;
        if (method_exists($this, $setter)) {
            $this->$setter($value);

            return;
        }

        throw new Exception('Setting unknown property: ' . get_class($this) . '::' . $name);
    }

    public function __isset($name)
    {

        return isset($this->$name);
    }

}