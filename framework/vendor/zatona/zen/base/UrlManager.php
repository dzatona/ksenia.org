<?php

namespace zen\base;

use Zen;

class UrlManager extends Component
{
    public function init()
    {
        parent::init();
    }

    public function parseRequest($request)
    {
        $pathInfo = trim($request->getPathInfo(), '/');

        $path = explode('/',$pathInfo);

        // Controller/action
        if (count($path) > 1) {
            return [array_shift($path) . '/' . array_shift($path), $path];
        } elseif (count($path) == 1 ) {
            if ($action = array_shift($path)) {
                return [ $action . '/index' , []];
            } else
                return ['site/index', []];
        }

        return false;
    }



}