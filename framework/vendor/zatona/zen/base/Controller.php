<?php
namespace zen\base;

use Zen;

class Controller extends Component {

    public $id = 'controller';
    // Base layout
    public $layout = null;

    /**
     * @param $action
     * @param $params
     * @return Response|false
     */
    public function runAction($action, $params)
    {
        if ($method = $this->resolveAction($action)) {
            return call_user_func_array([$this, $method],$params);
        }
        return false;
    }


    /**
     * Resolves action's method name in this controller
     * @param $action string Action name
     * @return null|string Controller's method name or null if no such method
     */
    protected function resolveAction($action)
    {
        $action = 'action' . str_replace(' ', '', ucwords(str_replace('-', ' ', $action)));

        if (strpos($action, '-') !== false || !method_exists($this, $action)) {
            return null;
        }

        return $action;
    }

    public function render($view, $params = [])
    {

        /** @var View $viewComponent */
        $viewComponent = Zen::$app->getComponent('view');

        $content =  $viewComponent->render($view, $params, $this);

        return $content;
    }

    public function getViewsPath($baseViewsPath = 'views')
    {
        return Zen::$app->getBasePath() . '/'.$baseViewsPath.'/' . $this->id;
    }

    public function getLayoutPath($baseLayoutsPath = 'views/layouts')
    {
        return $this->layout ? (Zen::$app->getBasePath() . '/' . $baseLayoutsPath. '/' . $this->layout) : false;

    }

    public function redirect($location = '/')
    {
        Zen::$app->getComponent('response')->redirect($location);
    }

}