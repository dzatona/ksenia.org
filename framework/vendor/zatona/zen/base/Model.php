<?php

namespace zen\base;

use Exception;

class Model extends Component {

    // Model attributes
    public $_attributes = [];


    public function init()
    {
        parent::init();
        $this->_attributes = $this->setAttributes();
    }

    public function setAttributes()
    {
        return [];
    }

    public function getAttributes()
    {
        return $this->_attributes;
    }

    /**
     * Returns model's attribute text label according to model's settigs
     * @param $attribute string Attribute name
     * @return string Attribute's text label
     */
    public function getAttributeLabel($attribute)
    {
        return isset($this->_attributes[$attribute]['label']) ? $this->_attributes[$attribute]['label'] : ucfirst($attribute);
    }

    /**
     * @param $attribute string Attribute name
     * @return string|bool Attribute type
     */
    public function getAttributeType($attribute)
    {
        return  isset($this->_attributes[$attribute]['type']) ? $this->_attributes[$attribute]['type'] : false;
    }

    public function __get($name)
    {
        if ($this->hasAttribute($name)) {
            return $this->getAttributeValue($name);
        }

        return parent::__get($name);
    }

    public function __set($name, $value)
    {
        if ($this->hasAttribute($name)) {
            $this->setAttributeValue($name, $value);
        } else {
            parent::__set($name, $value);
        }
    }

    public function __isset($name)
    {
        if ($this->hasAttribute($name)) {
            return $this->isSetAttribute($name);
        } else {
            parent::__isset($name);
        }
    }

    public function hasAttribute($name)
    {
        return isset($this->_attributes[$name]);
    }

    public function getAttributeValue($name)
    {
        throw new Exception(get_called_class().'::getAttributeValue must be implemented');
    }

    public function setAttributeValue($name, $value)
    {
        throw new Exception(get_called_class().'::getAttributeValue must be implemented');
    }

    public function isSetAttribute($name)
    {
        throw new Exception(get_called_class().'::isSetAttribute must be implemented');
    }

    public function remove()
    {
        throw new Exception(get_called_class().'::remove must be implemented');
    }

    public function getDefaultValue($attribute)
    {
        if ($this->hasAttribute($attribute)) {
            return isset($_attributes[$attribute]['defaultValue']) ? $_attributes[$attribute]['defaultValue'] : null;
        }

        return null;
    }
}