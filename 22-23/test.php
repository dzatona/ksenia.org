<?php
include 'core.php';

$id = 0;
$dir = './files';
$a = array_slice(scandir($dir), 2);

$n = isset($_GET['num']) ? intval($_GET['num']) : false;

if ( false === $n || !isset($a[$n])) {
    header('HTTP/1.0 403 Forbidden');
	exit;
} 

$test_file = json_decode(file_get_contents($dir . '/' . $a[$n]));

if (isset($_POST['Answers'])) {
	
	$answers = $_POST['Answers'];
	$name = isset($_POST['Name']) ? $_POST['Name'] : false;
	if ($name) { 
		$right = 0; $total = count($test_file);
		foreach ($test_file as $idx => $test) {
			if (isset($answers[$idx]) && intval($answers[$idx]) ==  $test[2]) {
				$right++;
			}
		}

	    $im = new Imagick();
		$im->readImage('certificate.png');
		$draw = new ImagickDraw();

		$draw->setFont('./PT_Sans/PT_Sans-Web-BoldItalic.ttf');
        $draw->setFontSize( 30 );
        $draw->setTextAlignment(\Imagick::ALIGN_CENTER);
        $im->annotateImage($draw, 398, 295, 0, strip_tags(htmlspecialchars($name)));

		$draw->setFont('./PT_Sans/PT_Sans-Web-Regular.ttf');
		$draw->setFontSize( 15 );
        $draw->setTextAlignment(\Imagick::ALIGN_LEFT);
		$im->annotateImage($draw, 125, 420, 0, 'Успешно ответил на ' . $right .' из ' .  $total . ' вопросов теста и получил квалификацию «Специалист»');

		$output = $im->getimageblob();
  		$outputtype = $im->getFormat();

  		header("Content-type: $outputtype");
  		echo $output;
	} else {
		die('<span>Не заполнено имя</span><br />');
	}
}
?>
<!DOCTYPE HTML>
<html>
<head>
    <title>Система тестирования Ксении Затона</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
    <link rel="stylesheet" href="assets/css/main.css" />
    <link rel="stylesheet" href="assets/css/custom.css" />
    <!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
    <!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
</head>
<body>

<div id="wrapper">

    <div id="main">
        <div class="inner">

            <header id="header">
                <a href="index.php" class="logo"><strong>Система тестирования</strong> Ксении Затона</a>
                <ul class="icons">
                    <?php
                    if (isLogged()) {
                        echo '<li>Вы авторизованы как: <strong>' . $_SESSION['name'] . '. </strong><a href="core.php?logout">Выйти</a>.</li>';
                    } else {
                        echo '<li>Вы не авторизованы</li>';
                    }
                    ?>
                </ul>
            </header>

            <section id="banner">
                <div class="content">
                    <header>
                        <h2>Тест </h2>
                    </header>
                    <?php
                        echo '<form  action="test.php?num=' . $n . '" method="post">';
                        echo '<div class="row uniform">';
                        echo '<div class="12u 12u$(small)"><label for="Name">Ваше имя:</label><input type="text" name="Name" value="'.$_SESSION['name'].'" readonly /></div>';

                        foreach ($test_file as $idx => $test) {
                            list($question, $answers, $right) = $test;
                            echo '<div class="12u 12u$(small)"><h3>' . $question . '</h3></div>';

                            foreach($answers as $index => $answer) {
                                echo '<div class="12u 12u$(small)">';
                                echo '<input type="radio" id="test' . $id . '" name="Answers[' . $idx . ']" value="' . $index . '" /> ';
                                echo '<label for="test' . $id . '">' . $answer . '</label>';
                                echo '</div>';

                                $id++;
                            }
                        }

                        echo '<div class="12u 12u$(small)"><input type="submit" value="Пройти тест" /></div></form>';
                        echo '</div>';

                        ?>
            </section>

        </div>
    </div>

    <div id="sidebar">
        <div class="inner">

            <nav id="menu">
                <header class="major">
                    <h2>Меню</h2>
                </header>
                <ul>
                    <li><a href="index.php">Главная страница</a></li>
                    <?php
                    if (isLogged()) {
                        if (isset($_SESSION['login']) && $_SESSION['login'] == 'admin') {
                            echo '<li><a href="admin.php">Загрузить тест</a></li>';
                        }
                        echo '<li><a href="list.php">Список тестов</a></li>';
                    }
                    ?>
                </ul>
            </nav>

            <footer id="footer">
                <p class="copyright">&copy; 2016 Ксения Затона</p>
            </footer>

        </div>
    </div>

</div>

<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/skel.min.js"></script>
<script src="assets/js/util.js"></script>
<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
<script src="assets/js/main.js"></script>

</body>
</html>
