<?php
session_start();

function file_force_download($file) {
    if (file_exists($file)) {
        if (ob_get_level()) {
            ob_end_clean();
        }
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . basename($file));
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));
        readfile($file);
        exit;
    }
}

function isLogged()
{
    return !empty($_SESSION['login']);
}

function isRemembered() {
    if (isset($_COOKIE['login'])) {
        if (file_exists('db/' . $_COOKIE['login'] . '.json')) {
            $a = json_decode(file_get_contents('db/' . $_COOKIE['login'] . '.json'), $assoc = true);
            if ($_COOKIE['hash'] == md5($a['password'] . $_SERVER['REMOTE_ADDR'])) {
                $_SESSION['login'] = $a['login'];
                $_SESSION['name'] = $a['name'];
            }
        }
    }
}

function setError($msg)
{
    $_SESSION['error'] = $msg;
}

function setCaptchaError($msg)
{
    $_SESSION['captchaError'] = $msg;
}

function getError()
{
    return isset($_SESSION['error']) ? $_SESSION['error'] : '';
}

function getCaptchaError()
{
    return isset($_SESSION['captchaError']) ? $_SESSION['captchaError'] : '';
}

function clearError()
{
    unset($_SESSION['error']);
    unset($_SESSION['captchaError']);
}

if (isset($_GET['deltest'])) {
    if (isset($_SESSION['login']) ? $_SESSION['login'] == 'admin' : false)  {
        unlink('./files/' . $_GET['deltest']);
        header('Location: list.php');
    } else {
        header('HTTP/1.0 403 Forbidden');
        exit;
    }
}

if (isset($_GET['logout'])) {
    session_start();
    setcookie('login', '', time()+604800, '/');
    setcookie('password', '', time()+604800, '/');
    setcookie('hash', '', time()+604800, '/');
    session_destroy();
    header('Location: index.php');
}

class Blocker {
    static function checkIP() {
        if (!file_exists('db/ip_base/' . $_SERVER['REMOTE_ADDR'] . '.json')) {
            $array = [
                'l_attempts' => 0,
                'l_captcha_attempts' => 0,
                'blocked' => 0,
                'blockTime' => 0
            ];
            file_put_contents('db/ip_base/' . $_SERVER['REMOTE_ADDR'] . '.json', json_encode($array));
        } else {
            $array = json_decode(file_get_contents('db/ip_base/' . $_SERVER['REMOTE_ADDR'] . '.json'), $assoc = true);
            if ($array['l_attempts'] < 3) {
                ++$array['l_attempts'];
            } else {
                $_SESSION['show_captcha'] = 1;
                ++$array['l_captcha_attempts'];
                if ($array['l_captcha_attempts'] == 3) {
                    $array['blocked'] = 1;
                    $array['blockTime'] = time();
                }
            }
            file_put_contents('db/ip_base/' . $_SERVER['REMOTE_ADDR'] . '.json', json_encode($array));
        }
    }
    static function isBlocked() {
        if (file_exists('db/ip_base/' . $_SERVER['REMOTE_ADDR'] . '.json')) {
            $b = json_decode(file_get_contents('db/ip_base/' . $_SERVER['REMOTE_ADDR'] . '.json'), $assoc = true);
            if ($b['blocked'] == 1) {
                header('HTTP/1.0 403 Forbidden');
                echo 'Вы заблокированы на 1 минуту.';
                exit;
            }
        }
    }
    static function checkBan() {
        if (file_exists('db/ip_base/' . $_SERVER['REMOTE_ADDR'] . '.json')) {
            $b = json_decode(file_get_contents('db/ip_base/' . $_SERVER['REMOTE_ADDR'] . '.json'), $assoc = true);
            if ((time()-$b['blockTime']) >= 60) {
                if ($b['blocked'] != 0) {
                    $_SESSION['show_captcha'] = 0;
                    $b['l_attempts'] = 0;
                    $b['l_captcha_attempts'] = 0;
                    $b['blocked'] = 0;
                    $b['blockTime'] = 0;
                    file_put_contents('db/ip_base/' . $_SERVER['REMOTE_ADDR'] . '.json', json_encode($b));
                }
            }
        }
    }
    static function showCaptcha() {
        if (isset($_SESSION['show_captcha']) && $_SESSION['show_captcha'] == 1) {
            echo '<div class="g-recaptcha" data-sitekey="6LfHfxEUAAAAAB-d5085NfV5CSuPfQXc-C9nFlNr"></div>';
        }
    }
    static function clearCounter() {
        $_SESSION['show_captcha'] = 0;
        $array = json_decode(file_get_contents('db/ip_base/' . $_SERVER['REMOTE_ADDR'] . '.json'), $assoc = true);
        $array['l_attempts'] = 0;
        $array['l_captcha_attempts'] = 0;
        $array['blocked'] = 0;
        $array['blockTime'] = 0;
        if (file_exists('db/ip_base/' . $_SERVER['REMOTE_ADDR'] . '.json')) {
            file_put_contents('db/ip_base/' . $_SERVER['REMOTE_ADDR'] . '.json', json_encode($array));
        }
    }
}