<?php
include 'core.php';
Blocker::checkBan();
Blocker::isBlocked();
isRemembered();
?>
<!DOCTYPE HTML>
<html>
<head>
    <title>Система тестирования Ксении Затона</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
    <link rel="stylesheet" href="assets/css/main.css" />
    <link rel="stylesheet" href="assets/css/custom.css" />
    <!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
    <!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
    <script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body>

<div id="wrapper">

    <div id="main">
        <div class="inner">

            <header id="header">
                <a href="index.php" class="logo"><strong>Система тестирования</strong> Ксении Затона</a>
                <ul class="icons">
                    <?php
                        if (isLogged()) {
                            echo '<li>Вы авторизованы как: <strong>' . $_SESSION['name'] . '. </strong><a href="core.php?logout">Выйти</a>.</li>';
                        } else {
                            echo '<li>Вы не авторизованы</li>';
                        }
                    ?>
                </ul>
            </header>

            <section id="banner">
                <div class="content">

                    <?php

                    if (isLogged()) {
                        echo <<<HTML
                        <header>
                            <h2>Здравствуйте!</h2>
                        </header>
                            <p>Я рада приветствовать Вас на сайте своей системы онлайн-тестирования базовых знаний об окружающей нас реальности. Для начала тестирования, пожалуйста, выберите пункт меню «Список тестов» слева или нажмите кнопку «Перейти к тестам» ниже. Удачи!</p>
                            <ul class="actions">
                                <li><a href="list.php" class="button big">Перейти к тестам</a></li>
                            </ul>
HTML;
                    } else {
                        echo <<<HTML
                        <header>
                            <h2>Авторизация</h2>
                        </header>
                            <p>Для доступа к системе тестирования Вам необходимо авторизоваться. Введите Ваш логин и пароль в форме ниже или войдите как гость<sup>1</sup>.</p>
                            <div class="box">
                                <form method="post" action="auth.php">                            
                                    <div class="row uniform">
                                        <div class="6u 12u$(xsmall)">
                                            <label for="login">Логин:</label>
                                            <input type="text" name="login" id="login" value="" placeholder="">
                                        </div>
                                        <div class="6u 12u$(xsmall)">
                                            <label for="password">Пароль:</label>
                                            <input type="password" name="password" id="password" value="" placeholder="">
                                        </div>
                                    </div>
									
HTML;

                        if (getError()) {
                            echo '<div class="row uniform error"><div class="12u$ 12u$(small)"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>ОШИБКА: </strong>' . getError() . '</div></div>';
                            clearError();
                        }

                        Blocker::showCaptcha();

                        if (getCaptchaError()) {
                            echo '<div class="row uniform error"><div class="12u$ 12u$(small)"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>ОШИБКА: </strong>' . getCaptchaError() . '</div></div>';
                            clearError();
                        }

                        echo <<<HTML
									<div class="row uniform">
                                        <div class="6u$ 12u$(small)">
                                            <input type="checkbox" id="remember" name="remember">
                                            <label for="remember">Запомнить меня</label>
                                        </div>
									</div>
									</div>
								    <ul class="actions">
                                        <button class="small" type="submit">Авторизоваться</button>
                                        <span style="margin: 0 14px; font-size: 11px; font-weight: bold;">или</span>
                                        <li><a href="auth.php?login=guest&password=guest" class="button special small">Войти как гость</a></li>
                                    </ul>	
                                </form>
                                <p class="smalll"><sup>1</sup> — опция «Запомнить меня» для гостя не работает.</p>
                            
HTML;

                    }
                    ?>
                </div>
                <span class="image object">
                    <img src="images/intro.png" alt="" />
                </span>
            </section>

        </div>
    </div>

    <div id="sidebar">
        <div class="inner">

            <nav id="menu">
                <header class="major">
                    <h2>Меню</h2>
                </header>
                <ul>
                    <li><a href="index.php">Главная страница</a></li>
                    <?php
                    if (isLogged()) {
                        if (isset($_SESSION['login']) && $_SESSION['login'] == 'admin') {
                            echo '<li><a href="admin.php">Загрузить тест</a></li>';
                        }
                        echo '<li><a href="list.php">Список тестов</a></li>';
                    }
                    ?>
                </ul>
            </nav>

            <footer id="footer">
                <p class="copyright">&copy; 2016 Ксения Затона</p>
            </footer>

        </div>
    </div>

</div>

<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/skel.min.js"></script>
<script src="assets/js/util.js"></script>
<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
<script src="assets/js/main.js"></script>

</body>
</html>