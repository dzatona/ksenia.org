<?php
include 'core.php';

if (!isLogged()) {
    header('HTTP/1.0 403 Forbidden');
    exit;
}

$dir = './files/';
$a = array_slice(scandir($dir), 2);

?>
<!DOCTYPE HTML>
<html>
<head>
    <title>Список тестов</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
    <link rel="stylesheet" href="assets/css/main.css" />
    <link rel="stylesheet" href="assets/css/custom.css" />
    <!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
    <!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
</head>
<body>

<div id="wrapper">

    <div id="main">
        <div class="inner">

            <header id="header">
                <a href="index.php" class="logo"><strong>Система тестирования</strong> Ксении Затона</a>
                <ul class="icons">
                    <?php
                    if (isLogged()) {
                        echo '<li>Вы авторизованы как: <strong>' . $_SESSION['name'] . '. </strong><a href="core.php?logout">Выйти</a>.</li>';
                    } else {
                        echo '<li>Вы не авторизованы</li>';
                    }
                    ?>
                </ul>
            </header>

            <section id="banner">
                <div class="content">
                    <header>
                        <h2>Список тестов</h2>
                    </header>
                        <?php
                        if(empty($a)) {
                            if (isLogged()) {
                                if ($_SESSION['login'] == 'admin') {
                                    echo '<p>Тесты не найдены, <a href="admin.php">загрузить</a>.</p>';
                                } else {
                                    echo '<p>Тесты не найдены.</p>';
                                }
                            }
                        } else {

                            if ($_SESSION['login'] == 'admin') {
                                echo '<div class="table-wrapper"><table class="alt"><thead>';
                                echo '<tr><th>Название теста</th><th>Опции теста</th></tr></thead><tbody>';
                                foreach ($a as $index => $filename) {
                                    echo '<tr><td><a href="test.php?num=' . $index . '">Тест №' . ($index+1) . '</a></td>';
                                    echo '<td><i class="fa fa-trash" aria-hidden="true"></i> <a href="core.php?deltest='. $filename .'">Удалить</a></td></tr>';
                                }
                                echo '</tbody></table></div>';
                            } else {
                                echo '<div class="table-wrapper"><table class="alt"><thead>';
                                echo '<tr><th>Название теста</th></tr></thead><tbody>';
                                foreach ($a as $index => $filename) {
                                    echo '<tr><td><a href="test.php?num=' . $index . '">Тест №' . ($index+1) . '</a></td>';
                                }
                                echo '</tbody></table></div>';
                            }
                        }
                        ?>

            </section>

        </div>
    </div>

    <div id="sidebar">
        <div class="inner">

            <nav id="menu">
                <header class="major">
                    <h2>Меню</h2>
                </header>
                <ul>
                    <li><a href="index.php">Главная страница</a></li>
                    <?php
                    if (isLogged()) {
                        if (isset($_SESSION['login']) && $_SESSION['login'] == 'admin') {
                            echo '<li><a href="admin.php">Загрузить тест</a></li>';
                        }
                        echo '<li><a href="list.php">Список тестов</a></li>';
                    }
                    ?>
                </ul>
            </nav>

            <footer id="footer">
                <p class="copyright">&copy; 2016 Ксения Затона</p>
            </footer>

        </div>
    </div>

</div>

<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/skel.min.js"></script>
<script src="assets/js/util.js"></script>
<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
<script src="assets/js/main.js"></script>

</body>
</html>