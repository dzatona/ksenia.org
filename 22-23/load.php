<?php
include 'core.php';
$upload_dir = './files';

if(isset($_FILES["filename"]) && is_uploaded_file($_FILES["filename"]["tmp_name"]))
{
    move_uploaded_file($_FILES["filename"]["tmp_name"], $upload_dir . '/' . $_FILES["filename"]["name"]);
    header('Location: list.php');
} else {
    setError('Выберите файл для загрузки!');
    header('Location: admin.php');
}