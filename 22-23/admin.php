<?php
include 'core.php';

if (!isLogged() || $_SESSION['login'] !== 'admin') {
    header('HTTP/1.0 403 Forbidden');
    exit;
}

?>
<!DOCTYPE HTML>
<html>
<head>
    <title>Загрузка тестов</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
    <link rel="stylesheet" href="assets/css/main.css" />
    <link rel="stylesheet" href="assets/css/custom.css" />
    <!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
    <!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
</head>
<body>

<div id="wrapper">

    <div id="main">
        <div class="inner">

            <header id="header">
                <a href="index.php" class="logo"><strong>Система тестирования</strong> Ксении Затона</a>
                <ul class="icons">
                    <?php
                    if (isLogged()) {
                        echo '<li>Вы авторизованы как: <strong>' . $_SESSION['name'] . '. </strong><a href="logout.php">Выйти</a>.</li>';
                    } else {
                        echo '<li>Вы не авторизованы</li>';
                    }
                    ?>
                </ul>
            </header>

            <section id="banner">
                <div class="content">
                    <header>
                        <h2>Загрузить тест</h2>
                    </header>
                    <form enctype="multipart/form-data" action="load.php" method="POST">
                        <div class="row uniform">
                        <input type="hidden" name="MAX_FILE_SIZE" value="30000" />
                        <input name="filename" type="file" />
                        </div>
                        <?php
                        if (getError()) {
                            echo '<div class="error" style="margin-bottom: 18px;"><div class="12u$ 12u$(small)"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>ОШИБКА: </strong>' . getError() . '</div></div>';
                            clearError();
                        }
                        ?>
                        <input type="submit" value="Загрузить" />
                        <a style="margin-left: 18px;" href="download.php?file=sample.json" class="button">Скачать образец теста</a>
                    </form>

            </section>

        </div>
    </div>

    <div id="sidebar">
        <div class="inner">

            <nav id="menu">
                <header class="major">
                    <h2>Меню</h2>
                </header>
                <ul>
                    <li><a href="index.php">Главная страница</a></li>
                    <?php
                        if ($_SESSION['login'] == 'admin') {
                            echo '<li><a href="admin.php">Загрузить тест</a></li>';
                        }
                    ?>
                    <li><a href="list.php">Список тестов</a></li>
                </ul>
            </nav>

            <footer id="footer">
                <p class="copyright">&copy; 2016 Ксения Затона</p>
            </footer>

        </div>
    </div>

</div>

<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/skel.min.js"></script>
<script src="assets/js/util.js"></script>
<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
<script src="assets/js/main.js"></script>

</body>
</html>