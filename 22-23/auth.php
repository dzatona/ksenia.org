<?php
include 'core.php';
$path = 'db/';

if (isset($_POST['g-recaptcha-response'])) {
    $captcha = $_POST['g-recaptcha-response'];
}

if ($_SESSION['show_captcha'] == 1) {
    if (!$captcha) {
        setCaptchaError('Подтвердите, что вы не робот!');
        header('Location: index.php');
        exit;
    }

    $response = json_decode(file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=6LfHfxEUAAAAACxLc97QAu7f6NolG6JnGr8jeomm&response=' . $captcha . '&remoteip=' . $_SERVER['REMOTE_ADDR']), true);

    if($response['success'] == false) {
        setCaptchaError('Неверная капча.');
        header('Location: index.php');
        exit;
    } else {
        if (isset($_REQUEST['login']) && isset($_REQUEST['password'])) {
            if (file_exists($path . $_REQUEST['login'] . '.json')) {
                $a = json_decode(file_get_contents($path . $_REQUEST['login'] . '.json'), $assoc = true);
                if ($a['login'] == $_REQUEST['login'] && md5($_REQUEST['password'].$a['login']) == $a['password']) {

                    $_SESSION['login'] = $a['login'];
                    $_SESSION['name'] = $a['name'];

                    if (isset($_REQUEST['remember']) == 'on') {
                        setcookie('login', $a['login'], time()+604800, '/');
                        setcookie('hash', md5($a['password'] . $_SERVER['REMOTE_ADDR']), time()+604800, '/');
                    }

                    /*
                     * Blocker Engine
                     */

                    Blocker::clearCounter();

                    header('Location: index.php');

                } else {

                    /*
                     * Blocker Engine
                     */

                    Blocker::checkIP();

                    setError('Неверный пароль.');
                    header('Location: index.php');
                }
            } else {

                /*
                 * Blocker Engine
                 */

                Blocker::checkIP();

                setError('Такого пользователя не существует.');
                header('Location: index.php');
            }
        } else {
            header('HTTP/1.0 403 Forbidden');
        }
    }
} else {
    if (isset($_REQUEST['login']) && isset($_REQUEST['password'])) {
        if (file_exists($path . $_REQUEST['login'] . '.json')) {
            $a = json_decode(file_get_contents($path . $_REQUEST['login'] . '.json'), $assoc = true);
            if ($a['login'] == $_REQUEST['login'] && md5($_REQUEST['password'].$a['login']) == $a['password']) {

                $_SESSION['login'] = $a['login'];
                $_SESSION['name'] = $a['name'];

                if (isset($_REQUEST['remember']) == 'on') {
                    setcookie('login', $a['login'], time()+604800, '/');
                    setcookie('hash', md5($a['password'] . $_SERVER['REMOTE_ADDR']), time()+604800, '/');
                }

                /*
                 * Blocker Engine
                 */

                Blocker::clearCounter();

                header('Location: index.php');

            } else {

                /*
                 * Blocker Engine
                 */

                Blocker::checkIP();

                setError('Неверный пароль.');
                header('Location: index.php');
            }
        } else {

            /*
             * Blocker Engine
             */

            Blocker::checkIP();

            setError('Такого пользователя не существует.');
            header('Location: index.php');
        }
    } else {
        header('HTTP/1.0 403 Forbidden');
    }
}