<?php

$a = new PDO('mysql:host=localhost;dbname=ksu;charset=utf8', 'ksu', 'password');

function pdoSet($allowed, &$values, $source = array()) {
    $set = '';
    $values = array();
    if (!$source) $source = &$_POST;
    foreach ($allowed as $field) {
        if (isset($source[$field])) {
            $set.="`".str_replace("`","``",$field)."`". "=:$field, ";
            $values[$field] = $source[$field];
        }
    }
    return substr($set, 0, -2);
}

if (isset($_REQUEST['action']) && !empty($_REQUEST['action'] == 'add' && !empty($_REQUEST['description']))) {
    $source = [
        'id' => NULL,
        'description' => strip_tags($_REQUEST['description']),
        'is_done' => '0',
        'date_added' => date('Y-m-d H:i:s')
    ];

    $allowed = array("id", "description", "is_done", "date_added");
    $sql = "INSERT INTO tasks SET " . pdoSet($allowed, $values, $source);
    $stm = $a->prepare($sql);

    if ($stm->execute($values)) {
        header('Location: index.php');
    }
}

if (!empty($_REQUEST['id']) && $_REQUEST['action'] == 'delete') {
    $sql = "DELETE FROM tasks WHERE id =  :id";
    $stmt = $a->prepare($sql);
    $stmt->bindParam(':id', $_REQUEST['id'], PDO::PARAM_INT);

    if ($stmt->execute()) {
        header('Location: index.php');
    }
}

if (!empty($_REQUEST['id']) && $_REQUEST['action'] == 'complete') {
    $sql = "UPDATE tasks SET is_done = 1 WHERE id = :id";
    $stmt = $a->prepare($sql);
    $stmt->bindParam(':id', $_REQUEST['id'], PDO::PARAM_INT);

    if ($stmt->execute()) {
        header('Location: index.php');
    }
}

if (!empty($_REQUEST['id']) && $_REQUEST['action'] == 'uncomplete') {
    $sql = "UPDATE tasks SET is_done = 0 WHERE id = :id";
    $stmt = $a->prepare($sql);
    $stmt->bindParam(':id', $_REQUEST['id'], PDO::PARAM_INT);

    if ($stmt->execute()) {
        header('Location: index.php');
    }
}

if (isset($_REQUEST['description']) && isset($_REQUEST['action']) && $_REQUEST['action'] == 'edit') {
    $sql = "UPDATE tasks SET description = :description WHERE id = :id";
    $stmt = $a->prepare($sql);
    $stmt->bindParam(':description', strip_tags($_REQUEST['description']), PDO::PARAM_STR);
    $stmt->bindParam(':id', $_REQUEST['id'], PDO::PARAM_INT);

    if ($stmt->execute()) {
        header('Location: index.php');
    }

}

if (!empty($_REQUEST['id'])) {
    $queryId = 'SELECT description FROM tasks WHERE id = ' . $_REQUEST['id'];
    $c = $a->query($queryId);
    $x = $c->fetch();
}

switch (isset($_REQUEST['action']) ? $_REQUEST['action'] : false) {
    case 'sortbyname':
        $query = 'SELECT * FROM tasks ORDER BY description';
        break;
    case 'sortbydate':
        $query = 'SELECT * FROM tasks ORDER BY date_added';
        break;
    case 'sortbystatus':
        $query = 'SELECT * FROM tasks ORDER BY is_done';
        break;
    default:
        $query = 'SELECT * FROM tasks';
        break;
}

$b = $a->query($query);

?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Список дел</title>
    <link rel="stylesheet" href="semantic.min.css" />
</head>
<body>

<div class="ui vertical masthead aligned segment">
    <div class="ui container">
        <form id="myform" action="index.php<?php if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'edit') {
            echo '?action=edit';
        } else {
            echo '?action=add';
        } ?>" method="post">
            <div class="ui left icon input">
                <input type="text" name="description" placeholder="<?php if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'edit') {
                    echo $x['description'];
                } else {
                    echo 'Введите задачу';
                } ?>">
                <i class="file icon"></i>
            </div>
            <button type="submit" class="ui labeled icon button primary">
                <?php if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'edit') {
                    echo '<i class="save icon"></i> Сохранить задачу';
                } else {
                    echo '<i class="plus icon"></i> Добавить задачу';
                } ?>
            </button>
            <div style="float: right;">
                <div style="display: inline-block"><strong>Сортировать по:</strong></div>
                <a class="ui labeled icon button mini" href="index.php?action=sortbyname"><i class="sort icon"></i>описанию</a>
                <a class="ui labeled icon button mini" href="index.php?action=sortbydate"><i class="sort icon"></i>дате добавления</a>
                <a class="ui labeled icon button mini" href="index.php?action=sortbystatus"><i class="sort icon"></i>статусу</a>
            </div>
            <?php if (isset($_REQUEST['id'])) echo '<input type="hidden" name="id" value="' . intval($_REQUEST['id']) . '">' ?>
        </form>
    </div>

    <div class="ui container">
        <table class="ui celled table" style="margin-top: 15px;">
            <thead>
            <tr>
                <th>Задача</th>
                <th style="width: 157px;">Дата добавления</th>
                <th style="width: 113px;">Статус</th>
                <th style="width: 424px;">Действия</th>
            </tr>
            </thead>
            <?php
            while ($row = $b->fetch()) {
                echo '<tr>';
                echo '<td>' . $row['description'] . '</td>';
                echo '<td>' . $row['date_added'] . '</td>';
                echo '<td>';
                    if ($row['is_done'] == 0) {
                        echo '<div class="ui label basic yellow">В процессе</div>';
                    } else {
                        echo '<div class="ui label basic green">Выполнена</div>';
                    }
                echo '</td>';
                echo '<td>';
                if ($row['is_done'] == 0) {
                    echo '<a class="ui labeled icon button mini" href="index.php?id=' . $row['id'] . '&action=complete"><i class="check icon"></i> Выполнить</a>';
                } else {
                    echo '<a class="ui labeled icon button mini" href="index.php?id=' . $row['id'] . '&action=uncomplete"><i class="undo icon"></i> Взять в работу</a>';
                };
                echo '<a class="ui labeled icon button mini" href="index.php?id=' . $row['id'] . '&action=edit"><i class="pencil icon"></i> Редактировать</a><a class="ui labeled icon button red mini" href="index.php?id=' . $row['id'] . '&action=delete"><i class="trash icon"></i> Удалить</a></td>';
                echo '</tr>';
            }
            ?>
        </table>
    </div>
</div>
</body>
</html>